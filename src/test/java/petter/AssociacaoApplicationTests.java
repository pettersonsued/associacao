package petter;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import petter.associacao.api.controller.CidadeController;
import petter.associacao.api.controller.DistritoController;
import petter.associacao.api.controller.EnderecoController;
import petter.associacao.api.controller.RuaController;
import petter.associacao.domain.model.CidadeEntity;
import petter.associacao.domain.model.DistritoEntity;
import petter.associacao.domain.model.EnderecoEntity;
import petter.associacao.domain.model.RuaEntity;
import petter.associacao.domain.repository.CidadeRepository;
import petter.associacao.domain.repository.DistritoRepository;
import petter.associacao.domain.repository.EnderecoRepository;
import petter.associacao.domain.repository.RuaRepository;
import petter.associacao.domain.service.CidadeService;
import petter.associacao.domain.service.DistritoService;
import petter.associacao.domain.service.EnderecoService;
import petter.associacao.domain.service.RuaService;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;


//@RunWith(MockitoJUnitRunner.class)
//@RunWith(SpringRunner.class)
@IntegrationTest
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@DisplayName("AssociacaoApplicationTests")
public class AssociacaoApplicationTests {





	/////////////////////////////////////////////////TESTES TESTES///////////////////////////////////////////////////



	@Test
	 public void contextLoads() {
		DistritoEntity distritoEntity = getDistrito();
		System.out.println(distritoEntity);
	}

	public CidadeEntity getCidade(){
		Long cidadeId = 1L;
		CidadeEntity cidadeEntity =new CidadeEntity();
		cidadeEntity.setId(cidadeId);
		cidadeEntity.setNome("Palhoça");
		cidadeEntity.setEstado("SC");
		cidadeEntity.setDistritos(mockListDistritos());
		return cidadeEntity;
	}

	public DistritoEntity getDistrito(){
		DistritoEntity distritoEntity =new DistritoEntity();
		distritoEntity.setId(1L);
		distritoEntity.setNome("Breja");
		distritoEntity.setCidade(getCidade());
		distritoEntity.setRuas(mockListRuas());
		return distritoEntity;
	}

	public RuaEntity getRua(){
		RuaEntity ruaEntity =new RuaEntity();
		ruaEntity.setId(1L);
		ruaEntity.setLogradouro("Pascoal Mazilli");
		ruaEntity.setCep("88133-650");
		ruaEntity.setDistrito(getDistrito());
		return ruaEntity;
	}

	public EnderecoEntity getEndereco(){
		EnderecoEntity enderecoEntity = new EnderecoEntity();
		enderecoEntity.setId(1L);
		enderecoEntity.setRua(getRua());
		enderecoEntity.setNumero(345);
		return enderecoEntity;
	}

	public List<CidadeEntity> mockListCidades(){
		List<CidadeEntity> cidades = new ArrayList<>();
		CidadeEntity c = new CidadeEntity();
		c.setId(1L);
		c.setNome("Palhoça");
		c.setEstado("SC");
		c.setDistritos(mockListDistritos());
		cidades.add(c);
		CidadeEntity c1 = new CidadeEntity();
		c1.setId(2L);
		c1.setNome("São José");
		c.setEstado("SC");
		c.setDistritos(mockListDistritos());
		cidades.add(c1);
		CidadeEntity c2 = new CidadeEntity();
		c2.setId(3L);
		c2.setNome("Florianópolis");
		c2.setEstado("SC");
		c2.setDistritos(mockListDistritos());
		cidades.add(c2);
		return  cidades;
	}

	public List<DistritoEntity> mockListDistritos(){
		List<DistritoEntity> distritos = new ArrayList<>();
		DistritoEntity d = new DistritoEntity();
		d.setId(1L);
		d.setNome("Jardim Eldorado");
		d.setCidade(null);
		d.setRuas(mockListRuas());
		distritos.add(d);
		DistritoEntity d1 = new DistritoEntity();
		d1.setId(2L);
		d1.setNome("Breja");
		d1.setCidade(null);
		d1.setRuas(mockListRuas());
		distritos.add(d1);
		DistritoEntity d2 = new DistritoEntity();
		d2.setId(3L);
		d2.setNome("Frei");
		d2.setCidade(null);
		d2.setRuas(mockListRuas());
		distritos.add(d2);
		return distritos;
	}

	public List<RuaEntity> mockListRuas(){
		List<RuaEntity> ruas = new ArrayList<>();
		RuaEntity r1 = new RuaEntity();
		r1.setId(1L);
		r1.setLogradouro("Das sete facadas");
		r1.setCep("88133-660");
		r1.setDistrito(null);
		ruas.add(r1);
		RuaEntity r = new RuaEntity();
		r.setId(2L);
		r.setLogradouro("trese de maio");
		r.setCep("88133-640");
		r.setDistrito(null);
		ruas.add(r);
		RuaEntity r2 = new RuaEntity();
		r2.setId(3L);
		r2.setLogradouro("Pascoal Mazilli");
		r2.setCep("88133-650");
		r2.setDistrito(null);
		ruas.add(r2);
		return  ruas;
	}

	public List<EnderecoEntity> mockListEnderecos(){
		List<EnderecoEntity> enderecos = new ArrayList<>();
		EnderecoEntity e1 = new EnderecoEntity();
		e1.setId(1L);
		e1.setRua(getRua());
		e1.setNumero(551);
		enderecos.add(e1);
		EnderecoEntity e2 = new EnderecoEntity();
		e2.setId(2L);
		e2.setRua(getRua());
		e2.setNumero(552);
		enderecos.add(e2);
		EnderecoEntity e3 = new EnderecoEntity();
		e3.setId(3L);
		e3.setRua(getRua());
		e3.setNumero(553);
		enderecos.add(e3);
		return  enderecos;
	}


}
