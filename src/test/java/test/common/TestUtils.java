package test.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.hateoas.mediatype.hal.Jackson2HalModule;

import java.io.IOException;
import java.net.URL;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;

public class TestUtils {

    public static final String UTF_8 = "UTF-8";

    public static String toJson(Object object) throws IOException {
        ObjectMapper mapper = getHalEnabledMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        return mapper.writeValueAsString(object);
    }

    public static <T> T getFromResource(String fileName, TypeReference<T> type) throws IOException {
        ObjectMapper mapper = getHalEnabledMapper();
        URL resource = TestUtils.class.getResource(fileName.concat(".json"));
        return mapper.readValue(resource, type);
    }

    public static <T> T getFromStringJSON(String content, TypeReference<T> type) throws IOException {
        ObjectMapper mapper = getHalEnabledMapper();
        return mapper.readValue(content, type);
    }

    private static ObjectMapper getHalEnabledMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new Jackson2HalModule());
        mapper.configure(FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        return mapper;
    }
}
