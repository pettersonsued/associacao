package test.api.controller;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import petter.AssociacaoApplicationTests;
import petter.TesteUnitario;
import petter.associacao.api.controller.EnderecoController;
import petter.associacao.domain.model.CidadeEntity;
import petter.associacao.domain.model.DistritoEntity;
import petter.associacao.domain.model.EnderecoEntity;
import petter.associacao.domain.model.RuaEntity;
import petter.associacao.domain.repository.EnderecoRepository;
import petter.associacao.domain.service.EnderecoService;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@TesteUnitario
@DisplayName("EnderecoControllerTest")
public class EnderecoControllerTest extends AssociacaoApplicationTests {

    @InjectMocks
    private EnderecoController enderecoController;

    @Mock
    public EnderecoService enderecoService;

    @Mock
    public EnderecoRepository enderecoRepository;



    //////////////////////////////////////TESTES  TESTES/////////////////////////////////////////////////




    @Test
    @DisplayName("deve listar todos os enderecos")
    public void listarTodasOsEnderecos() {
        when(enderecoService.listar()).thenReturn(mockListEnderecos());

        List<EnderecoEntity> enderecoEntityList = enderecoController.listar();

        assertNotNull(enderecoEntityList);
        assertEquals(3, mockListEnderecos().size());
        verify(enderecoService, times(1)).listar();
    }

    @Test
    public void deveSalvarUmEndereco() {
        when(enderecoService.salvar(getEndereco())).thenReturn(getEndereco());

        EnderecoEntity endereco = enderecoController.adicionar(getEndereco());

        assertNotNull(endereco);
        assertThat(HttpStatus.CREATED);
        verify(enderecoService, times(1)).salvar(getEndereco());
    }

    /*@Test
    public void deveAtualizarUmEnderecoCasoIdRetorneTrue() {
        Long enderecoId = 1L;
        EnderecoEntity endereco = getEndereco();
        endereco.setId(enderecoId);
        endereco.setNumero(444);
        when(enderecoService.existsById(enderecoId)).thenReturn(true);
        when(enderecoRepository.save(endereco)).thenReturn(endereco);

        ResponseEntity<EnderecoEntity> responseEntity = enderecoController.atualizar(enderecoId, endereco);
        EnderecoEntity logradouro = enderecoRepository.save(endereco);

        assertNotNull(logradouro);
        assertEquals(enderecoId, logradouro.getId());
        assertEquals("Pascoal Mazilli", logradouro.getRua().getLogradouro());
        assertNotNull(responseEntity);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
        verify(enderecoService, times(1)).existsById(enderecoId);
        verify(enderecoRepository, times(1)).save(endereco);
    }*/

   /* @Test
    public void naoDeveAtualizarUmEnderecoCasoIdRetorneFalse() {
        Long enderecoId = 2L;
        when(enderecoService.existsById(enderecoId)).thenReturn(false);

        ResponseEntity<EnderecoEntity> responseEntity = enderecoController.atualizar(enderecoId, getEndereco());

        assertNotNull(responseEntity);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(404);
        verify(enderecoService, times(1)).existsById(enderecoId);
        verify(enderecoRepository, times(0)).save(getEndereco());
    }*/

    @Test
    public void deveRemoverUmEnderecoComIdExistente(){
        Long enderecoId = 1L;
        when(enderecoService.existsById(enderecoId)).thenReturn(true);

        ResponseEntity<Void> response = enderecoController.remover(enderecoId);
        enderecoRepository.deleteById(enderecoId);

        assertNotNull(response);
        assertThat(response.getStatusCodeValue()).isEqualTo(204);
        verify(enderecoService, times(1)).existsById(enderecoId);
        verify(enderecoRepository, times(1)).deleteById(enderecoId);
    }

    @Test
    public void naoDeveRemoverUmEnderecoComIdNaoExistente(){
        Long enderecoId = 2L;

        when(enderecoService.existsById(enderecoId)).thenReturn(false);
        ResponseEntity<Void> response = enderecoController.remover(enderecoId);

        assertNotNull(response);
        assertThat(response.getStatusCodeValue()).isEqualTo(404);
        verify(enderecoService, times(1)).existsById(enderecoId);
        verify(enderecoRepository, times(0)).deleteById(enderecoId);
    }

}
