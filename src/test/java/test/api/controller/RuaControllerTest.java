package test.api.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import petter.AssociacaoApplicationTests;
import petter.TesteUnitario;
import petter.associacao.api.controller.RuaController;
import petter.associacao.domain.model.RuaEntity;
import petter.associacao.domain.repository.RuaRepository;
import petter.associacao.domain.service.RuaService;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@TesteUnitario
@DisplayName("RuaControllerTest")
public class RuaControllerTest extends AssociacaoApplicationTests {

    @InjectMocks
    private RuaController ruaController;

    @Mock
    public RuaRepository ruaRepository;

    @Mock
    public RuaService ruaService;




    //////////////////////////////////////TESTES  TESTES/////////////////////////////////////////////////



    @Test
    public void listarTodasAsRuas() {
        when(ruaController.listar()).thenReturn(mockListRuas());

        List<RuaEntity> ruaEntityList = ruaController.listar();

        assertNotNull(ruaEntityList);
        verify(ruaService, times(1)).listar();
    }

    @Test
    public void buscarRuaPorId() {
        Long ruaId = 1L;

        when(ruaService.findByRuaPorId(ruaId)).thenReturn(Optional.of(getRua()));
        ResponseEntity<RuaEntity> response = ruaController.buscarRuaPorId(ruaId);

        assertNotNull(response);
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        verify(ruaService, times(1)).findByRuaPorId(ruaId);
    }

    @Test
    public void buscarRuaPorNome() {
        String name = "Pascoal Mazilli";

        when(ruaService.findByRuaPorNome(name)).thenReturn(Optional.of(getRua()));
        ResponseEntity<RuaEntity> response = ruaController.buscarRuaPorNome(name);

        assertNotNull(response);
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        verify(ruaService, times(1)).findByRuaPorNome(name);
    }

    @Test
    public void deveSalvarUmaRua() {
        when(ruaService.salvar(getRua())).thenReturn(getRua());

        RuaEntity rua = ruaController.adicionar(getRua());

        assertNotNull(rua);
        assertThat(HttpStatus.CREATED);
        verify(ruaService, times(1)).salvar(getRua());
    }

    @Test
    public void deveAtualizarUmaRuaCasoIdRetorneTrue() {
        Long ruaId = 1L;
        RuaEntity rua = getRua();
        rua.setId(ruaId);

        when(ruaRepository.save(getRua())).thenReturn(getRua());
        when(ruaService.existsById(ruaId)).thenReturn(true);
        ResponseEntity<RuaEntity> responseEntity = ruaController.atualizar(ruaId, getRua());
        RuaEntity logradouro = ruaRepository.save(rua);

        assertNotNull(logradouro);
        assertEquals(ruaId, logradouro.getId());
        assertNotNull(responseEntity);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
        verify(ruaService, times(1)).existsById(ruaId);
        verify(ruaRepository, times(1)).save(rua);
    }

    @Test
    public void naoDeveAtualizarUmaRuaCasoIdRetorneFalse() {
        Long ruaId = 2L;

        when(ruaService.existsById(ruaId)).thenReturn(false);
        ResponseEntity<RuaEntity> responseEntity = ruaController.atualizar(ruaId, getRua());

        assertNotNull(responseEntity);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(404);
        verify(ruaService, times(1)).existsById(ruaId);
        verify(ruaRepository, times(0)).save(getRua());
    }

    @Test
    public void deveRemoverUmaRuaComIdExistente(){
        Long ruaId = 1L;

        when(ruaService.existsById(ruaId)).thenReturn(true);
        ResponseEntity<Void> response = ruaController.remover(ruaId);
        ruaRepository.deleteById(ruaId);

        assertNotNull(response);
        assertThat(response.getStatusCodeValue()).isEqualTo(204);
        verify(ruaService, times(1)).existsById(ruaId);
        verify(ruaRepository, times(1)).deleteById(ruaId);
    }

    @Test
    public void naoDeveRemoverUmaRuaComIdNaoExistente(){
        Long ruaId = 2L;

        when(ruaService.existsById(ruaId)).thenReturn(false);
        ResponseEntity<Void> response = ruaController.remover(ruaId);

        assertNotNull(response);
        assertThat(response.getStatusCodeValue()).isEqualTo(404);
        verify(ruaService, times(1)).existsById(ruaId);
        verify(ruaRepository, times(0)).deleteById(ruaId);
    }
}
