package test.api.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import petter.AssociacaoApplicationTests;
import petter.TesteUnitario;
import petter.associacao.api.controller.DistritoController;
import petter.associacao.domain.model.DistritoEntity;
import petter.associacao.domain.repository.DistritoRepository;
import petter.associacao.domain.service.DistritoService;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@TesteUnitario
@DisplayName("DistritoControllerTest")
public class DistritoControllerTest extends AssociacaoApplicationTests {

    @InjectMocks
    private DistritoController distritoController;

    @Mock
    public DistritoRepository distritoRepository;

    @Mock
    public DistritoService distritoService;




    //////////////////////////////////////TESTES  TESTES/////////////////////////////////////////////////




    @Test
    public void listarTodasOsDistritos() {
        when(distritoController.listar()).thenReturn(mockListDistritos());

        List<DistritoEntity> distritoEntityList = distritoController.listar();

        assertNotNull(distritoEntityList);
        verify(distritoService, times(1)).listar();
    }

    @Test
    public void buscarUmDistritoPorId() {
        Long distritoId = 1L;

        when(distritoService.findByDistritoPorId(distritoId)).thenReturn(Optional.of(getDistrito()));
        ResponseEntity<DistritoEntity> response = distritoController.buscarDistritoPorId(distritoId);

        assertNotNull(response);
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        verify(distritoService, times(1)).findByDistritoPorId(distritoId);
    }

    @Test
    public void buscarUmDistritoPorNome() {
        String name = "Breja";

        when(distritoService.findByDistritoPorNome(name)).thenReturn(Optional.of(getDistrito()));
        ResponseEntity<DistritoEntity> response = distritoController.buscarDistritoPorNome(name);

        assertNotNull(response);
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        verify(distritoService, times(1)).findByDistritoPorNome(name);
    }

    @Test
    public void deveSalvarUmDistrito() {
        when(distritoService.salvar(getDistrito())).thenReturn(getDistrito());
        DistritoEntity distrito = distritoController.adicionar(getDistrito());

        assertNotNull(distrito);
        assertThat(HttpStatus.CREATED);
        verify(distritoService, times(1)).salvar(getDistrito());
    }

    @Test
    public void deveAtualizarUmDistritoCasoIdRetorneTrue() {
        Long distritoId = 1L;
        DistritoEntity distrito = getDistrito();
        distrito.setId(distritoId);

        when(distritoRepository.save(getDistrito())).thenReturn(getDistrito());
        when(distritoService.existsById(distritoId)).thenReturn(true);
        ResponseEntity<DistritoEntity> responseEntity = distritoController.atualizar(distritoId, getDistrito());
        DistritoEntity bairro = distritoRepository.save(distrito);

        assertNotNull(bairro);
        assertEquals(distritoId, bairro.getId());
        assertNotNull(responseEntity);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
        verify(distritoService, times(1)).existsById(distritoId);
        verify(distritoRepository, times(1)).save(distrito);
    }

    @Test
    public void naoDeveAtualizarUmDistritoCasoIdRetorneFalse() {
        Long distritoId = 2L;

        when(distritoService.existsById(distritoId)).thenReturn(false);
        ResponseEntity<DistritoEntity> responseEntity = distritoController.atualizar(distritoId, getDistrito());

        assertNotNull(responseEntity);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(404);
        verify(distritoService, times(1)).existsById(distritoId);
        verify(distritoRepository, times(0)).save(getDistrito());
    }

    @Test
    public void deveRemoverUmDistritoComIdExistente(){
        Long distritoId = 1L;

        when(distritoService.existsById(distritoId)).thenReturn(true);
        ResponseEntity<Void> response = distritoController.remover(distritoId);
        distritoRepository.deleteById(distritoId);

        assertNotNull(response);
        assertThat(response.getStatusCodeValue()).isEqualTo(204);
        verify(distritoService, times(1)).existsById(distritoId);
        verify(distritoRepository, times(1)).deleteById(distritoId);
    }

    @Test
    public void naoDeveRemoverUmDistritoComIdNaoExistente(){
        Long distritoId = 2L;

        when(distritoService.existsById(distritoId)).thenReturn(false);
        ResponseEntity<Void> response = distritoController.remover(distritoId);

        assertNotNull(response);
        assertThat(response.getStatusCodeValue()).isEqualTo(404);
        verify(distritoService, times(1)).existsById(distritoId);
        verify(distritoRepository, times(0)).deleteById(distritoId);
    }

}
