package test.api.controller;


import org.junit.Test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.mock.mockito.MockBean;


import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.web.client.RestTemplate;

import petter.IntegrationTest;
import petter.associacao.api.controller.CidadeController;
import petter.associacao.domain.model.CidadeEntity;
import petter.associacao.domain.model.DistritoEntity;
import petter.associacao.domain.model.EnderecoEntity;
import petter.associacao.domain.model.RuaEntity;
import petter.associacao.domain.repository.CidadeRepository;
import petter.associacao.domain.service.CidadeService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

//import static com.sun.xml.internal.stream.writers.XMLStreamWriterImpl.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
//import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;
import static test.common.TestUtils.UTF_8;

//@RunWith(MockitoJUnitRunner.class)
@RunWith(SpringRunner.class)
@IntegrationTest
@Sql(config = @SqlConfig(encoding = UTF_8))
public class CidadeControllerIT {

    private static final Long ID_LAGES = 100L;
    private static final String CIDADE_LISTAR = "/cidades";
    private static final String CIDADE_BUSCAR_ID = "/cidades/{id}";
    //private static final String CIDADE_BUSCAR_ID = "/cidades";
    private static final String CIDADE_BUSCAR_NOME = "/cidades/search?nome={nome}";
    private static final String CIDADE_ADICIONAR = "/cidades";

    @Mock
    private CidadeService cidadeService;

    @MockBean
    private CidadeRepository cidadeRepository;

    @InjectMocks
    CidadeController cidadeController;


   /*@Autowired
    private CidadeService cidadeService;

   @Autowired
   private CidadeRepository cidadeRepository;

   @Autowired
   CidadeController cidadeController;*/

   @Autowired
   private MockMvc mockMvc;

   @Autowired
  private RestTemplate restTemplate;

   //@Autowired
   //private RestTemplateBuilder restTemplateBuilder;

    @BeforeEach
    public void setup(){
        standaloneSetup(cidadeController).build();
    }

    private ResultMatcher totalDeElementosIgualA(int quantidadeEsperada) {
        return jsonPath("$.totalElements", is(quantidadeEsperada));
    }




    //////////////////////////////////////TESTES TESTES/////////////////////////////////////////////////////////




    @Test
    public void deveListarTodasAsCidades() throws Exception {
        var list = mockListCidades();
        when(cidadeRepository.findAll()).thenReturn(list);

        mockMvc.perform(get(CIDADE_LISTAR))
                .andDo(print())
                .andExpect(status().isOk());

        assertNotNull(list);
        assertEquals(3, list.size());
        verify(cidadeRepository,times(1)).findAll();
    }

    @Test
    public void buscarCidadePorId_200() throws Exception {
        Long id = 1L;

        when(cidadeRepository.findById(id)).thenReturn(Optional.of(getCidade()));
        mockMvc.perform(get(CIDADE_BUSCAR_ID, id))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.nome", is("Palhoça")))
                .andExpect(jsonPath("$.estado", is("SC")));

        verify(cidadeRepository, times(1)).findById(id);
    }

    @Test
    public void naoDeveBuscarCidadePorId_404() throws Exception {
        Long id = 2L;

        mockMvc.perform(get(CIDADE_BUSCAR_ID, id))
                .andDo(print())
                .andExpect(status().isNotFound());

        verify(cidadeRepository, times(1)).findById(id);
    }

    /*@Test
    public void buscarCidadePorId_2003() throws Exception {
        CidadeEntity cidade= getCidade();
        when(cidadeRepository.findById(cidade.getId())).thenReturn(Optional.of(cidade));

        ResponseEntity<CidadeEntity> responseEntity = restTemplate.getForEntity("/cidades/{id}", CidadeEntity.class, cidade.getId());

        assertNotNull(responseEntity);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
        verify(cidadeRepository,times(1)).findById(cidade.getId());
    }*/

    public CidadeEntity getCidade(){
        Long cidadeId = 1L;
        CidadeEntity cidadeEntity =new CidadeEntity();
        cidadeEntity.setId(cidadeId);
        cidadeEntity.setNome("Palhoça");
        cidadeEntity.setEstado("SC");
        cidadeEntity.setDistritos(mockListDistritos());
        return cidadeEntity;
    }

    public DistritoEntity getDistrito(){
        DistritoEntity distritoEntity =new DistritoEntity();
        distritoEntity.setId(1L);
        distritoEntity.setNome("Breja");
        distritoEntity.setCidade(getCidade());
        distritoEntity.setRuas(mockListRuas());
        return distritoEntity;
    }

    public RuaEntity getRua(){
        RuaEntity ruaEntity =new RuaEntity();
        ruaEntity.setId(1L);
        ruaEntity.setLogradouro("Pascoal Mazilli");
        ruaEntity.setCep("88133-650");
        ruaEntity.setDistrito(getDistrito());
        return ruaEntity;
    }

    public EnderecoEntity getEndereco(){
        EnderecoEntity enderecoEntity = new EnderecoEntity();
        enderecoEntity.setId(1L);
        enderecoEntity.setRua(getRua());
        enderecoEntity.setNumero(345);
        return enderecoEntity;
    }

    public List<CidadeEntity> mockListCidades(){
        List<CidadeEntity> cidades = new ArrayList<>();
        CidadeEntity c = new CidadeEntity();
        c.setId(1L);
        c.setNome("Palhoça");
        c.setEstado("SC");
        c.setDistritos(mockListDistritos());
        cidades.add(c);
        CidadeEntity c1 = new CidadeEntity();
        c1.setId(2L);
        c1.setNome("São José");
        c.setEstado("SC");
        c.setDistritos(mockListDistritos());
        cidades.add(c1);
        CidadeEntity c2 = new CidadeEntity();
        c2.setId(3L);
        c2.setNome("Florianópolis");
        c2.setEstado("SC");
        c2.setDistritos(mockListDistritos());
        cidades.add(c2);
        return  cidades;
    }

    public List<DistritoEntity> mockListDistritos(){
        List<DistritoEntity> distritos = new ArrayList<>();
        DistritoEntity d = new DistritoEntity();
        d.setId(1L);
        d.setNome("Jardim Eldorado");
        d.setCidade(null);
        d.setRuas(mockListRuas());
        distritos.add(d);
        DistritoEntity d1 = new DistritoEntity();
        d1.setId(2L);
        d1.setNome("Breja");
        d1.setCidade(null);
        d1.setRuas(mockListRuas());
        distritos.add(d1);
        DistritoEntity d2 = new DistritoEntity();
        d2.setId(3L);
        d2.setNome("Frei");
        d2.setCidade(null);
        d2.setRuas(mockListRuas());
        distritos.add(d2);
        return distritos;
    }

    public List<RuaEntity> mockListRuas(){
        List<RuaEntity> ruas = new ArrayList<>();
        RuaEntity r1 = new RuaEntity();
        r1.setId(1L);
        r1.setLogradouro("Das sete facadas");
        r1.setCep("88133-660");
        r1.setDistrito(null);
        ruas.add(r1);
        RuaEntity r = new RuaEntity();
        r.setId(2L);
        r.setLogradouro("trese de maio");
        r.setCep("88133-640");
        r.setDistrito(null);
        ruas.add(r);
        RuaEntity r2 = new RuaEntity();
        r2.setId(3L);
        r2.setLogradouro("Pascoal Mazilli");
        r2.setCep("88133-650");
        r2.setDistrito(null);
        ruas.add(r2);
        return  ruas;
    }

    public List<EnderecoEntity> mockListEnderecos(){
        List<EnderecoEntity> enderecos = new ArrayList<>();
        EnderecoEntity e1 = new EnderecoEntity();
        e1.setId(1L);
        e1.setRua(getRua());
        e1.setNumero(551);
        enderecos.add(e1);
        EnderecoEntity e2 = new EnderecoEntity();
        e2.setId(2L);
        e2.setRua(getRua());
        e2.setNumero(552);
        enderecos.add(e2);
        EnderecoEntity e3 = new EnderecoEntity();
        e3.setId(3L);
        e3.setRua(getRua());
        e3.setNumero(553);
        enderecos.add(e3);
        return  enderecos;
    }
}
