package test.api.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import petter.AssociacaoApplicationTests;
import petter.IntegrationTest;
import petter.TesteUnitario;
import petter.associacao.api.controller.CidadeController;
import petter.associacao.domain.model.CidadeEntity;
import petter.associacao.domain.repository.CidadeRepository;
import petter.associacao.domain.service.CidadeService;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@IntegrationTest
@DisplayName("CidadeControllerTest")
public class CidadeControllerTest extends AssociacaoApplicationTests {

    @InjectMocks
    private CidadeController cidadeController;

    @Mock
    public CidadeRepository cidadeRepository;

    @Mock
    public CidadeService cidadeService;





    //////////////////////////////////////TESTES  TESTES/////////////////////////////////////////////////




    @Test
    public void listarTodasAsCidades() {
        when(cidadeController.listar()).thenReturn(mockListCidades());

        List<CidadeEntity> cidadeEntityList = cidadeController.listar();

        assertNotNull(cidadeEntityList);
        verify(cidadeService, times(1)).listar();
    }

    @Test
    public void buscarCidadePorId() {
        Long cidadeId = 1L;

        when(cidadeService.findByCidadePorId(cidadeId)).thenReturn(Optional.of(getCidade()));
        ResponseEntity<CidadeEntity> response = cidadeController.buscarCidadePorId(cidadeId);

        assertNotNull(response);
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        verify(cidadeService, times(1)).findByCidadePorId(cidadeId);
    }

    @Test
    public void buscarCidadePorNome() {
        String name = "Palhoça";

        when(cidadeService.findByCidadePorNome(name)).thenReturn(Optional.of(getCidade()));
        ResponseEntity<CidadeEntity> response = cidadeController.buscarCidadePorNome(name);

        assertNotNull(response);
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        verify(cidadeService, times(1)).findByCidadePorNome(name);
    }

    @Test
    public void deveSalvarUmaCidade() {
        when(cidadeService.salvar(getCidade())).thenReturn(getCidade());
        CidadeEntity cidade = cidadeController.adicionar(getCidade());

        assertNotNull(cidade);
        assertThat(HttpStatus.CREATED);
        verify(cidadeService, times(1)).salvar(getCidade());
    }

    @Test
    public void deveAtualizarUmaCidadeCasoIdRetorneTrue() {
        Long cidadeId = 1L;
        CidadeEntity cidade = getCidade();
        cidade.setId(cidadeId);

        when(cidadeRepository.save(getCidade())).thenReturn(getCidade());
        when(cidadeService.existsById(cidadeId)).thenReturn(true);
        ResponseEntity<CidadeEntity> responseEntity = cidadeController.atualizar(cidadeId, getCidade());
        CidadeEntity city = cidadeRepository.save(cidade);

        assertNotNull(city);
        assertEquals(cidadeId, city.getId());
        assertNotNull(responseEntity);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
        verify(cidadeService, times(1)).existsById(cidadeId);
        verify(cidadeRepository, times(1)).save(cidade);
    }

    @Test
    public void naoDeveAtualizarUmaCidadeCasoIdRetorneFalse() {
        Long cidadeId = 2L;

        when(cidadeService.existsById(cidadeId)).thenReturn(false);
        ResponseEntity<CidadeEntity> responseEntity = cidadeController.atualizar(cidadeId, getCidade());

        assertNotNull(responseEntity);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(404);
        verify(cidadeService, times(1)).existsById(cidadeId);
        verify(cidadeRepository, times(0)).save(getCidade());
    }

    @Test
    public void deveRemoverUmaCidadeComIdExistente(){
        Long cidadeId = 1L;

        when(cidadeService.existsById(cidadeId)).thenReturn(true);
        ResponseEntity<Void> response = cidadeController.remover(cidadeId);
        cidadeRepository.deleteById(cidadeId);

        assertNotNull(response);
        assertThat(response.getStatusCodeValue()).isEqualTo(204);
        verify(cidadeService, times(1)).existsById(cidadeId);
        verify(cidadeRepository, times(1)).deleteById(cidadeId);
    }

    @Test
    public void naoDeveRemoverUmaCidadeComIdNaoExistente(){
        Long cidadeId = 2L;

        when(cidadeService.existsById(cidadeId)).thenReturn(false);
        ResponseEntity<Void> response = cidadeController.remover(cidadeId);

        assertNotNull(response);
        assertThat(response.getStatusCodeValue()).isEqualTo(404);
        verify(cidadeService, times(1)).existsById(cidadeId);
        verify(cidadeRepository, times(0)).deleteById(cidadeId);
    }
}
