package test.domain.service;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import petter.AssociacaoApplicationTests;
import petter.TesteUnitario;
import petter.associacao.api.controller.EnderecoController;
import petter.associacao.domain.exception.CadastroException;
import petter.associacao.domain.model.EnderecoEntity;
import petter.associacao.domain.model.RuaEntity;
import petter.associacao.domain.repository.EnderecoRepository;
import petter.associacao.domain.repository.RuaRepository;
import petter.associacao.domain.service.EnderecoService;
import petter.associacao.domain.service.RuaService;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@TesteUnitario
@DisplayName("EnderecoServiceTest")
public class EnderecoServiceTest extends AssociacaoApplicationTests {

    @InjectMocks
    private EnderecoService enderecoService;

    @Mock
    public EnderecoController enderecoController;
    @Mock
    public EnderecoRepository enderecoRepository;

    @Mock
    public RuaService ruaService;





    ///////////////////////////////////////////////TESTES TESTES//////////////////////////////////////////



    @Test
    public void deveRetonarUmaListaDeEnderecos(){
        //given
        var enderecos = new LinkedList<>(mockListEnderecos());

        //when
        when(enderecoRepository.findAll()).thenReturn(enderecos);
        List<EnderecoEntity> enderecosList = enderecoService.listar();

        //then
        assertNotNull(enderecosList);
        verify(enderecoRepository, times(1)).findAll();
    }

    @Test
    public void deveSalvarUmEnderecoQuandoIdDaRuaExiste(){
        String name = "Pascoal Mazilli";

        when(ruaService.findByRuaPorId(getRua().getId())).thenReturn(Optional.of(getRua()));
        when(enderecoRepository.save(getEndereco())).thenReturn(getEndereco());

        EnderecoEntity endereco = enderecoService.salvar(getEndereco());

        assertNotNull(endereco);
        assertEquals("Pascoal Mazilli", endereco.getRua().getLogradouro());
        verify(ruaService,times(1)).findByRuaPorId(getRua().getId());
        verify(enderecoRepository,times(1)).save(getEndereco());
    }

    @Test
    public void naoDeveSalvarUmEnderecoQuandoIdDaRuaNaoExiste(){
        when(ruaService.findByRuaPorId(getRua().getId())).thenReturn(Optional.empty());
        //when(ruaService.findByRuaPorId(getRua().getId())).thenThrow(new CadastroException("Rua com ID " + getRua().getId()+ " não Existe"));

        boolean existeNome = ruaService.findByRuaPorId(getRua().getId()).stream()
                .anyMatch(ruaExistente -> !ruaExistente.getId().equals(getRua().getId()));
        //EnderecoEntity endereco = enderecoService.salvar(getEndereco());

        assertNotNull(existeNome);
        assertEquals(false, existeNome);
        verify(ruaService,times(1)).findByRuaPorId(getRua().getId());
        verify(enderecoRepository,times(0)).save(getEndereco());
    }

    /*@Test
    public void deveAtualizarUmaEndereco(){
        when(enderecoRepository.save(getEndereco())).thenReturn(getEndereco());

        EnderecoEntity endereco = enderecoService.atualizar(getEndereco());

        assertNotNull(endereco);
        verify(enderecoRepository,times(1)).save(getEndereco());
    }*/

    @Test
    public void deveVerificarSeIdDeUmEnderecoExiste(){
        Long enderecoId = 1L;
        when(enderecoRepository.existsById(enderecoId)).thenReturn(true);

        boolean existe = enderecoService.existsById(enderecoId);

        assertNotNull(existe);
        assertEquals(true, existe);
        verify(enderecoRepository,times(1)).existsById(enderecoId);
    }

    @Test
    public void deveRemoverUmEndereco(){
        doNothing().when(enderecoRepository).deleteById(getRua().getId());

        boolean existe = enderecoService.excluir(getRua().getId());

        assertEquals(true, existe);
        verify(enderecoRepository,times(1)).deleteById(getRua().getId());
    }

}
