package test.domain.service;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import petter.AssociacaoApplicationTests;
import petter.TesteUnitario;
import petter.associacao.api.controller.CidadeController;
import petter.associacao.domain.model.CidadeEntity;
import petter.associacao.domain.repository.CidadeRepository;
import petter.associacao.domain.service.CidadeService;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@TesteUnitario
@DisplayName("CidadeServiceTest")
public class CidadeServiceTest extends AssociacaoApplicationTests {

    @InjectMocks
    private CidadeService cidadeService;

    @Mock
    public CidadeRepository cidadeRepository;

    @Mock
    public CidadeController cidadeController;




    ///////////////////////////////////////////////TESTES TESTES//////////////////////////////////////////



    @Test
    public void deveRetonarUmaListaDeCidade(){
        //given
        var cidades = new LinkedList<>(mockListCidades());

        //when
        when(cidadeService.listar()).thenReturn(cidades);
        List<CidadeEntity> cidadeList = cidadeService.listar();

        //then
        assertNotNull(cidadeList);
        verify(cidadeRepository, times(1)).findAll();
    }

    @Test
    public void deveBuscarUmaCidadePorId(){
        Long cidadeId = 1L;

        when(cidadeService.findByCidadePorId(cidadeId)).thenReturn(Optional.of(getCidade()));
        Optional<CidadeEntity> optional = cidadeService.findByCidadePorId(cidadeId);
        CidadeEntity cidade = optional.get();

        assertNotNull(cidade);
        verify(cidadeRepository,times(1)).findById(cidadeId);
    }

    @Test
    public void naoDeveBuscarUmaCidadePorId(){
        Long cidadeId = 1L;

        when(cidadeService.findByCidadePorId(cidadeId)).thenReturn(null);
        Optional<CidadeEntity> optional = cidadeService.findByCidadePorId(cidadeId);

        assertNull(optional);
        verify(cidadeRepository,times(1)).findById(cidadeId);
    }

    @Test
    public void deveBuscarUmaCidadePorNome(){
        String name = "Palhoça";

        when(cidadeService.findByCidadePorNome(name)).thenReturn(Optional.of(getCidade()));
        Optional<CidadeEntity> optional = cidadeService.findByCidadePorNome(name);
        CidadeEntity cidade = optional.get();

        assertNotNull(cidade);
        assertEquals("Palhoça", cidade.getNome());
        verify(cidadeRepository,times(1)).findByNome(name);
    }

    @Test
    public void naoDeveBuscarUmaCidadePorNome(){
        String name = "Palhoça";

        when(cidadeService.findByCidadePorNome(name)).thenReturn(null);
        Optional<CidadeEntity> optional = cidadeService.findByCidadePorNome(name);

        assertNull(optional);
        assertEquals(null, optional);
        verify(cidadeRepository,times(1)).findByNome(name);
    }

    @Test
    public void deveSalvarUmaCidadeNaoCadastrada(){
        String name = "Florianópolis";

        when(cidadeService.findByCidadePorNome(name)).thenReturn(Optional.of(getCidade()));
        when(cidadeRepository.save(getCidade())).thenReturn(getCidade());
        boolean existeNome = cidadeService.findByCidadePorNome(name).stream()
                .anyMatch(cidadeExistente -> cidadeExistente.getNome().equals(name));
        CidadeEntity cidade = cidadeRepository.save(getCidade());

        assertNotNull(existeNome);
        assertNotNull(cidade);
        assertEquals(false, existeNome);
        verify(cidadeRepository,times(1)).save(getCidade());
        verify(cidadeRepository,times(1)).findByNome(name);
    }

    @Test
    public void naoDeveSalvarUmaCidadeJaCadastrada(){
        String name = "Palhoça";

        when(cidadeService.findByCidadePorNome(name)).thenReturn(Optional.of(getCidade()));
        boolean existeNome = cidadeService.findByCidadePorNome(name).stream()
                .anyMatch(cidadeExistente -> cidadeExistente.getNome().equals(name));

        assertNotNull(existeNome);
        assertEquals(true, existeNome);
        verify(cidadeRepository,times(1)).findByNome(name);
        verify(cidadeRepository,times(0)).save(getCidade());
    }

    @Test
    public void deveAlualizarUmaCidade(){
        CidadeEntity cidadeEntity = getCidade();

        when(cidadeService.atualizar(cidadeEntity)).thenReturn(cidadeEntity);
        CidadeEntity cidade = cidadeService.atualizar(cidadeEntity);

        assertNotNull(cidade);
        verify(cidadeRepository,times(1)).save(cidadeEntity);
    }

    @Test
    public void deveVerificarSeIdDeUmaCidadeExiste(){
        Long cidadeId = 1L;

        when(cidadeService.existsById(cidadeId)).thenReturn(true);
        boolean existe = cidadeService.existsById(cidadeId);

        assertEquals(true, existe);
        verify(cidadeRepository,times(1)).existsById(cidadeId);
    }

    @Test
    public void deveRemoverUmaCidade(){

        doNothing().when(cidadeRepository).deleteById(getCidade().getId());
        boolean existe = cidadeService.excluir(getCidade().getId());


        assertEquals(true, existe);
        verify(cidadeRepository,times(1)).deleteById(getCidade().getId());
    }
}

