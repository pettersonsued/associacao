package test.domain.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import petter.AssociacaoApplicationTests;
import petter.TesteUnitario;
import petter.associacao.api.controller.DistritoController;
import petter.associacao.domain.model.CidadeEntity;
import petter.associacao.domain.model.DistritoEntity;
import petter.associacao.domain.model.RuaEntity;
import petter.associacao.domain.repository.DistritoRepository;
import petter.associacao.domain.service.DistritoService;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@TesteUnitario
@DisplayName("DistritoServiceTest")
public class DistritoServiceTest extends AssociacaoApplicationTests {



    @InjectMocks
    public DistritoService distritoService;

    @Mock
    public DistritoRepository distritoRepository;

    @Mock
    public DistritoController distritoController;




    ////////////////////////////////TESTES  TESTES/////////////////////////////////////////////////


    @Test
    public void deveRetonarUmaListaDeDistritos(){
        //given
        var distritos = new LinkedList<>(mockListDistritos());

        //when
        when(distritoService.listar()).thenReturn(distritos);
        List<DistritoEntity> distritoList = distritoService.listar();

        //then
        assertNotNull(distritoList);
        verify(distritoRepository, times(1)).findAll();
    }

    @Test
    public void deveBuscarUmDistritoPorId(){
        Long distritoId = 1L;

        when(distritoService.findByDistritoPorId(distritoId)).thenReturn(Optional.of(getDistrito()));
        Optional<DistritoEntity> optional = distritoService.findByDistritoPorId(distritoId);
        DistritoEntity distrito = optional.get();

        assertNotNull(distrito);
        verify(distritoRepository,times(1)).findById(distritoId);
    }

    @Test
    public void naoDeveBuscarUmDistritoPorId(){
        Long distritoId = 1L;

        when(distritoService.findByDistritoPorId(distritoId)).thenReturn(null);
        Optional<DistritoEntity> optional = distritoService.findByDistritoPorId(distritoId);

        assertNull(optional);
        verify(distritoRepository,times(1)).findById(distritoId);
    }

    @Test
    public void deveBuscarUmDistritoPorNome(){
        String name = "Breja";

        when(distritoService.findByDistritoPorNome(name)).thenReturn(Optional.of(getDistrito()));
        Optional<DistritoEntity> optional = distritoService.findByDistritoPorNome(name);
        DistritoEntity distrito = optional.get();

        assertNotNull(distrito);
        assertEquals("Breja", distrito.getNome());
        verify(distritoRepository,times(1)).findByNome(name);
    }

    @Test
    public void naoDeveBuscarUmDistritoPorNome(){
        String name = "Breja";

        when(distritoService.findByDistritoPorNome(name)).thenReturn(null);
        Optional<DistritoEntity> optional = distritoService.findByDistritoPorNome(name);

        assertNull(optional);
        verify(distritoRepository,times(1)).findByNome(name);
    }

    @Test
    public void deveSalvarUmDistritoNaoCadastrado(){
        String name = "Jardim Eldorado";

        when(distritoService.findByDistritoPorNome(name)).thenReturn(Optional.of(getDistrito()));
        when(distritoRepository.save(getDistrito())).thenReturn(getDistrito());
        boolean existeNome = distritoService.findByDistritoPorNome(name).stream()
                .anyMatch(distritoExistente -> distritoExistente.getNome().equals(name));
        DistritoEntity distrito = distritoRepository.save(getDistrito());

        assertNotNull(existeNome);
        assertNotNull(distrito);
        assertEquals(false, existeNome);
        verify(distritoRepository,times(1)).save(getDistrito());
        verify(distritoRepository,times(1)).findByNome(name);
    }

    @Test
    public void naoDeveSalvarUmaCidadeJaCadastrada(){
        String name = "Breja";

        when(distritoService.findByDistritoPorNome(name)).thenReturn(Optional.of(getDistrito()));
        boolean existeNome = distritoService.findByDistritoPorNome(name).stream()
                .anyMatch(distritoExistente -> distritoExistente.getNome().equals(name));

        assertNotNull(existeNome);
        assertEquals(true, existeNome);
        verify(distritoRepository,times(0)).save(getDistrito());
        verify(distritoRepository,times(1)).findByNome(name);
    }

    @Test
    public void deveAlualizarUmDistrito(){
        DistritoEntity distritoEntity = getDistrito();

        when(distritoService.atualizar(distritoEntity)).thenReturn(distritoEntity);
        DistritoEntity distrito = distritoService.atualizar(distritoEntity);

        assertNotNull(distrito);
        verify(distritoRepository,times(1)).save(distritoEntity);
    }

    @Test
    public void deveVerificarSeIdDeUmDistritoExiste(){
        Long distritoId = 1L;

        when(distritoService.existsById(distritoId)).thenReturn(true);
        boolean existe = distritoService.existsById(distritoId);

        assertEquals(true, existe);
        verify(distritoRepository,times(1)).existsById(distritoId);
    }

    @Test
    public void deveRemoverUmDistrito(){

        doNothing().when(distritoRepository).deleteById(getDistrito().getId());
        boolean existe = distritoService.excluir(getDistrito().getId());


        assertEquals(true, existe);
        verify(distritoRepository,times(1)).deleteById(getDistrito().getId());
    }
}
