package test.domain.service;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import petter.AssociacaoApplicationTests;
import petter.TesteUnitario;
import petter.associacao.api.controller.RuaController;
import petter.associacao.domain.model.CidadeEntity;
import petter.associacao.domain.model.RuaEntity;
import petter.associacao.domain.repository.RuaRepository;
import petter.associacao.domain.service.RuaService;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@TesteUnitario
@DisplayName("RuaServiceTest")
public class RuaServiceTest extends AssociacaoApplicationTests {

    @InjectMocks
    private RuaService ruaService;

    @Mock
    public RuaRepository ruaRepository;

    @Mock
    public RuaController ruaController;



    ///////////////////////////////////////////////TESTES TESTES//////////////////////////////////////////



    @Test
    public void deveRetonarUmaListaDeRuas(){
        //given
        var ruas = new LinkedList<>(mockListRuas());

        //when
        when(ruaService.listar()).thenReturn(ruas);
        List<RuaEntity> ruaList = ruaService.listar();

        //then
        assertNotNull(ruaList);
        verify(ruaRepository, times(1)).findAll();
    }

    @Test
    public void deveBuscarUmaRuaPorId(){
        Long ruaId = 1L;

        when(ruaService.findByRuaPorId(ruaId)).thenReturn(Optional.of(getRua()));
        Optional<RuaEntity> optional = ruaService.findByRuaPorId(ruaId);
        RuaEntity rua = optional.get();

        assertNotNull(rua);
        verify(ruaRepository,times(1)).findById(ruaId);
    }

    @Test
    public void naoDeveBuscarUmaRuaPorId(){
        Long ruaId = 1L;

        when(ruaService.findByRuaPorId(ruaId)).thenReturn(null);
        Optional<RuaEntity> optional = ruaService.findByRuaPorId(ruaId);

        assertNull(optional);
        verify(ruaRepository,times(1)).findById(ruaId);
    }

    @Test
    public void deveBuscarUmaRuaPorNome(){
        String name = "Pascoal Mazilli";

        when(ruaService.findByRuaPorNome(name)).thenReturn(Optional.of(getRua()));
        Optional<RuaEntity> optional = ruaService.findByRuaPorNome(name);
        RuaEntity rua = optional.get();

        assertNotNull(rua);
        assertEquals("Pascoal Mazilli", rua.getLogradouro());
        verify(ruaRepository,times(1)).findByLogradouro(name);
    }

    @Test
    public void naoDeveBuscarUmaRuaPorNome(){
        String name = "Trese de maio";

        when(ruaService.findByRuaPorNome(name)).thenReturn(null);
        Optional<RuaEntity> optional = ruaService.findByRuaPorNome(name);

        assertNull(optional);
        assertEquals(null, optional);
        verify(ruaRepository,times(1)).findByLogradouro(name);
    }

    @Test
    public void deveSalvarUmaRuaNaoCadastrada(){
        String name = "Padre José";

        when(ruaService.findByRuaPorNome(name)).thenReturn(Optional.of(getRua()));
        when(ruaRepository.save(getRua())).thenReturn(getRua());
        boolean existeNome = ruaService.findByRuaPorNome(name).stream()
                .anyMatch(ruaExistente -> ruaExistente.getLogradouro().equals(name));
        RuaEntity rua = ruaRepository.save(getRua());

        assertNotNull(existeNome);
        assertNotNull(rua);
        assertEquals(false, existeNome);
        verify(ruaRepository,times(1)).save(getRua());
        verify(ruaRepository,times(1)).findByLogradouro(name);
    }

    @Test
    public void naoDeveSalvarUmaRuaJaCadastrada(){
        String name = "Pascoal Mazilli";
        when(ruaService.findByRuaPorNome(name)).thenReturn(Optional.of(getRua()));

        boolean existeNome = ruaService.findByRuaPorNome(name).stream()
                .anyMatch(ruaExistente -> ruaExistente.getLogradouro().equals(name));

        assertNotNull(existeNome);
        assertEquals(true, existeNome);
        verify(ruaRepository,times(1)).findByLogradouro(name);
        verify(ruaRepository,times(0)).save(getRua());
    }

    @Test
    public void deveAtualizarUmaRua(){
        when(ruaRepository.save(getRua())).thenReturn(getRua());

        RuaEntity rua = ruaService.atualizar(getRua());

        assertNotNull(rua);
        verify(ruaRepository,times(1)).save(getRua());
    }

    @Test
    public void deveVerificarSeIdDeUmaRuaExiste(){
        Long ruaId = 1L;

        when(ruaService.existsById(ruaId)).thenReturn(true);
        boolean existe = ruaService.existsById(ruaId);

        assertEquals(true, existe);
        verify(ruaRepository,times(1)).existsById(ruaId);
    }

    @Test
    public void deveRemoverUmaRua(){
        doNothing().when(ruaRepository).deleteById(getRua().getId());
        boolean existe = ruaService.excluir(getRua().getId());

        assertEquals(true, existe);
        verify(ruaRepository,times(1)).deleteById(getRua().getId());
    }
}
