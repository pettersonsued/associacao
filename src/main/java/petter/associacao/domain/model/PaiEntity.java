package petter.associacao.domain.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import petter.associacao.api.ValidationGroups;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
//@PrimaryKeyJoinColumn(name="id")
public class PaiEntity {

    @NotNull(groups = ValidationGroups.PaiEntityId.class)
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @NotBlank
    @Size(max = 20)
    private String escolaridade;

    @NotNull
    @NotBlank
    @Size(max = 80)
    private String profissao;

    @NotNull
    @NotBlank
    @Size(max = 80)
    private String local_trabalho;

    @JsonIgnore
    @OneToMany(mappedBy = "pai", cascade = CascadeType.ALL)
    private List<AlunoEntity> list_alunos;

    @OneToOne
    private PessoaFisicaEntity pessoa;

}
