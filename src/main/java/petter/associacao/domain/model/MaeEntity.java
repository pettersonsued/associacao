package petter.associacao.domain.model;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import petter.associacao.api.ValidationGroups;

import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
//@PrimaryKeyJoinColumn(name = "id")
@Entity
public class MaeEntity {

	@NotNull(groups = ValidationGroups.MaeEntityId.class)
	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@NotBlank
	@Size(max = 20)
	private String escolaridade;
	
	@NotNull
	@NotBlank
	@Size(max = 80)
	private String profissao;
	
	@NotNull
	@NotBlank
	@Size(max = 80)
	private String local_trabalho;

	@OneToOne
	private PessoaFisicaEntity pessoa;

	@JsonIgnore
	@OneToMany(mappedBy = "mae", cascade = CascadeType.ALL)
	private List<AlunoEntity> list_alunos;

}
