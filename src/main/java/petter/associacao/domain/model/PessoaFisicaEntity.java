package petter.associacao.domain.model;

import java.sql.Date;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.validation.groups.ConvertGroup;
import javax.validation.groups.Default;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import petter.associacao.api.ValidationGroups;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity
@PrimaryKeyJoinColumn(name = "id")
public class PessoaFisicaEntity extends PessoaEntity{

	
	@NotNull
	@NotBlank
	@Size(max = 16)
	private String cpf;
	
	@NotNull
	@NotBlank
	@Size(max = 16)
	private String rg;
	
	@NotNull
	@NotBlank
	@Size(max = 20)
	private String sexo;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private Date dataNascimento;
}
