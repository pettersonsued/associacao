package petter.associacao.domain.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import petter.associacao.api.ValidationGroups;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class AlergiaEntity {

    @NotNull(groups = ValidationGroups.AlergiaEntityId.class)
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @NotBlank
    @Size(max = 80)
    private String nome;

    @NotNull
    @NotBlank
    @Size(max = 80)
    private String causa;

    @NotNull
    @NotBlank
    @Size(max = 80)
    private String grauPerigo;

    @JsonIgnore
    @ManyToMany
    private List<AlunoEntity> alunos;
}
