package petter.associacao.domain.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import petter.associacao.api.ValidationGroups;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.validation.groups.ConvertGroup;
import javax.validation.groups.Default;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class RegistroEntity {

    @NotNull(groups = ValidationGroups.RegistroEntityId.class)
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private Integer numeroLivro;

    @NotNull
    private Integer numeroFolha;

    @NotNull
    private Integer numeroTermo;

    @Valid
    @ConvertGroup(from = Default.class, to = ValidationGroups.CartorioEntityId.class)
    @NotNull
    //@NotBlank
    @OneToOne
    private CartorioEntity cartorio;
}
