package petter.associacao.domain.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import petter.associacao.api.ValidationGroups;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.validation.groups.ConvertGroup;
import javax.validation.groups.Default;
import java.sql.Date;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class  AlunoEntity {

    @NotNull(groups = ValidationGroups.DistritoEntityId.class)
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer matriculaAssociacao;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date dataMatriculaAssociacao;

    @NotNull
    @NotBlank
    @Size(max = 20)
    private String racaCor;

    private Integer matriculaEscola;

    @NotNull
    @NotBlank
    @Size(max = 20)
    private String serie;

    @NotNull
    @NotBlank
    @Size(max = 20)
    private String turno;

    @NotNull
    @NotBlank
    @Size(max = 20)
    private String situacaoAssociacao;

    @NotNull
    @NotBlank
    @Size(max = 20)
    private String bolsa;

    @OneToOne
    private PessoaFisicaEntity pessoa;

    @NotNull
    @NotBlank
    @Size(max = 30)
    private String orgaoExpedidorRg;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date dataExpedicaoRg;

    @Valid
    @ConvertGroup(from = Default.class, to = ValidationGroups.MaeEntityId.class)
    @NotNull
    @ManyToOne
    private MaeEntity mae;

    @Valid
    @ConvertGroup(from = Default.class, to = ValidationGroups.PaiEntityId.class)
    @NotNull
    @ManyToOne
    private PaiEntity pai;

    @Valid
    @ConvertGroup(from = Default.class, to = ValidationGroups.CidadeEntityId.class)
    @NotNull
    @ManyToOne
    private CidadeEntity cidade;

    @Valid
    @ConvertGroup(from = Default.class, to = ValidationGroups.RegistroEntityId.class)
    @NotNull
    @OneToOne
    private RegistroEntity registro;

    @Valid
    @ConvertGroup(from = Default.class, to = ValidationGroups.EscolaEntityId.class)
    @NotNull
    @ManyToOne
    private EscolaEntity escola;


    @ManyToMany
    private List<IrmaoEntity> irmaos;

    @ManyToMany
    private List<AlergiaEntity> alergias;

    @ManyToMany
    private List<ProblemaSaudeEntity> problemas;

}