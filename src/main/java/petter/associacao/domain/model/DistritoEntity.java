package petter.associacao.domain.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.validation.groups.ConvertGroup;
import javax.validation.groups.Default;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import petter.associacao.api.ValidationGroups;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class DistritoEntity {
	
	@NotNull(groups = ValidationGroups.DistritoEntityId.class)
	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	//@NotNull
	@NotBlank
	@Size(max = 80)
	private String nome;
	
	@Valid
	@ConvertGroup(from = Default.class, to = ValidationGroups.CidadeEntityId.class)
	@NotNull
	@ManyToOne
	private CidadeEntity cidade;

	//@Valid
	//@NotNull
	//@ConvertGroup(from = Default.class, to = ValidationGroups.ProdutoId.class)
	@JsonIgnore
	@OneToMany(mappedBy = "distrito", cascade = CascadeType.REMOVE)
	private List<RuaEntity> ruas;

	public DistritoEntity() {

	}

	public DistritoEntity(long l, String jardin_eldorado, String palhoça, List<RuaEntity> mockListRuas) {
	}
}
