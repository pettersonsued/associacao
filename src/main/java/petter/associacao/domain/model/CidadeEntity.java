package petter.associacao.domain.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import petter.associacao.api.ValidationGroups;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class CidadeEntity {
	
	@NotNull(groups = ValidationGroups.CidadeEntityId.class)
	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	
	//@NotNull
	@NotBlank
	@Size(max = 80)
	String nome;
	
	//@NotNull
	@NotBlank
	@Size(max = 6)
	String estado;
	
	//@Valid
	//@NotNull
	//@ConvertGroup(from = Default.class, to = ValidationGroups.ProdutoId.class)
	@JsonIgnore
	@OneToMany(mappedBy = "cidade", cascade = CascadeType.ALL)
	List<DistritoEntity> distritos;

	@JsonIgnore
	@OneToMany(mappedBy = "cidade")
	List<AlunoEntity> alunos;

	public CidadeEntity() {

	}

    public CidadeEntity(long l, String jardim_eldorado, String palhoça, List<DistritoEntity> mockListDistrito) {
    }
}
