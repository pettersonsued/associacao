package petter.associacao.domain.model;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import petter.associacao.api.ValidationGroups;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.validation.groups.ConvertGroup;
import javax.validation.groups.Default;
import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Inheritance(strategy = InheritanceType.JOINED)
@Entity
public class PessoaEntity implements Serializable {

    @NotNull(groups = ValidationGroups.PessoaEntityId.class)
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @NotBlank
    @Size(max = 80)
    private String nome;

    @NotNull
    @NotBlank
    @Size(max = 20)
    private String fone;

    @NotNull
    @NotBlank
    @Size(max = 80)
    @Email
    private String email;

    @Valid
    @ConvertGroup(from = Default.class, to = ValidationGroups.EnderecoEntityId.class)
    @NotNull
    //@NotBlank
    @OneToOne
    private EnderecoEntity endereco;
}
