package petter.associacao.domain.model;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.validation.groups.ConvertGroup;
import javax.validation.groups.Default;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import petter.associacao.api.ValidationGroups;

import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class RuaEntity {
	
	@NotNull(groups = ValidationGroups.RuaEntityId.class)
	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	//@NotNull
	@NotBlank
	@Size(max = 80)
	private String logradouro;
	
	//@NotNull
	@NotBlank
	@Size(max = 15)
	private String cep;
	
	@Valid
	@ConvertGroup(from = Default.class, to = ValidationGroups.DistritoEntityId.class)
	@NotNull
	@ManyToOne
	private DistritoEntity distrito;

	@JsonIgnore
	@OneToMany(mappedBy = "rua", cascade = CascadeType.REMOVE)
	private List<EnderecoEntity> enderecos;

	public RuaEntity(long l, String delfin_ribeiro, String s, DistritoEntity distritoEntity) {
	}

	public RuaEntity() {

	}
}
