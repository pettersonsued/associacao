package petter.associacao.domain.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import petter.associacao.api.ValidationGroups;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.validation.groups.ConvertGroup;
import javax.validation.groups.Default;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class CartorioEntity {

    @NotNull(groups = ValidationGroups.CartorioEntityId.class)
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private PessoaJuridicaEntity pessoa;

    /*@JsonIgnore
    @OneToMany(mappedBy = "cartorio", cascade = CascadeType.ALL)
    private List<LivroEntity> livros;

    @JsonIgnore
    @OneToMany(mappedBy = "cartorio", cascade = CascadeType.ALL)
    private List<AlunoEntity> alunos;*/
}
