package petter.associacao.domain.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import petter.associacao.api.ValidationGroups;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.sql.Date;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
//@PrimaryKeyJoinColumn(name="id")
public class AssociadoContribuinteEntity {

    @NotNull(groups = ValidationGroups.AssociadoContribuinteEntityId.class)
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date dataInicio;

    @NotNull
    private BigDecimal valor;

    @OneToOne
    private PessoaFisicaEntity pessoa;

}
