package petter.associacao.domain.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import petter.associacao.domain.exception.CadastroException;
import petter.associacao.domain.model.*;
import petter.associacao.domain.repository.AlunoRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class AlunoService {

      private AlunoRepository alunoRepository;
      private PaiService paiService;
      private MaeService maeService;
      private EscolaService escolaService;
      private CidadeService cidadeService;
      private RegistroService registroService;
      private EnderecoService enderecoService;
      private PessoaFisicaService pessoaFisicaService;
      private IrmaoService irmaoService;
      private ProblemaSaudeService problemaSaudeService;
      private AlergiaService alergiaService;


    //////////////////LISTA TODOS OS PAIS////////////////////////////////
    @Transactional
    public List<AlunoEntity> listar() {
        return alunoRepository.findAll();
    }

     ///////////// BUSCA OS PAIS POR ID/////////////////////////////
    @Transactional
    public Optional<AlunoEntity> findByAlunoPorId(Long alunoId) {
        return alunoRepository.findById(alunoId);
    }

    //////////// BUSCA OS PAIS POR NOME/////////////////////////////
    @Transactional
    public Optional<AlunoEntity> findByAlunoPorNome(String nome) {
        return alunoRepository.findByNome(nome);
    }


    ///////////////////////CADASTRA O ALUNO/////////////////////////
    public AlunoEntity salvar(AlunoEntity aluno){
        Optional<AlunoEntity> alunoEntity = alunoRepository.findByAlunoPorCpf(aluno.getPessoa().getCpf());
        if(alunoEntity.isPresent()){
            throw new CadastroException("Aluno Já Cadastrado  (" + alunoEntity.get().getId()+ ") Com Id ");
        }
        aluno.setPai(this.validaPai(aluno.getPai()));
        aluno.setMae(this.validaMae(aluno.getMae()));
        aluno.setEscola(this.validaEscola(aluno.getEscola()));
        aluno.setCidade(this.validaCidade(aluno.getCidade()));
        aluno.setPessoa(this.cadastrarPessoa(aluno.getPessoa()));
        aluno.setRegistro(this.cadastrarRegistro(aluno.getRegistro()));
        aluno.setIrmaos(this.validaIrmao(aluno.getIrmaos()));
        aluno.setProblemas(this.validaProblemaSaude(aluno.getProblemas()));
        aluno.setAlergias(this.validaAlergia(aluno.getAlergias()));
          return alunoRepository.save(aluno);
    }

    ///////////////////////ATUALIZA O ALUNO/////////////////////////

    @Transactional
    public AlunoEntity atualizar(AlunoEntity aluno) {
        Optional<AlunoEntity> optional = this.findByAlunoPorId(aluno.getId());
        if(optional.isEmpty()){
            throw new CadastroException("Aluno com esse ID  (" + aluno.getPessoa().getId()+ ") Não encontrado ");
        }
        aluno.setId(optional.get().getId());
        aluno.setRegistro(optional.get().getRegistro());
        aluno.setPessoa(pessoaFisicaService.atualizar(aluno.getPessoa()));
        aluno.setPai(this.validaPai(aluno.getPai()));
        aluno.setMae(this.validaMae(aluno.getMae()));
        aluno.setEscola(this.validaEscola(aluno.getEscola()));
        aluno.setCidade(this.validaCidade(aluno.getCidade()));
        aluno.setIrmaos(this.validaIrmao(aluno.getIrmaos()));
        aluno.setProblemas(this.validaProblemaSaude(aluno.getProblemas()));
        aluno.setAlergias(this.validaAlergia(aluno.getAlergias()));
        return alunoRepository.save(aluno);
    }

      private PaiEntity validaPai(PaiEntity pai){
        Optional<PaiEntity> optional = paiService.findByPaiPorId(pai.getId());
        if(optional.isEmpty()){
            throw new CadastroException("Pai com Nome  (" + pai.getPessoa().getNome()+") Não Cadastrado");
        }
        return optional.get();
      }

    private MaeEntity validaMae(MaeEntity mae){
        Optional<MaeEntity> optional = maeService.findByMaePorId(mae.getId());
        if(optional.isEmpty()){
            throw new CadastroException("Mãe com Nome  (" + mae.getPessoa().getNome()+") Não Cadastrado");
        }
        return optional.get();
    }

    private EscolaEntity validaEscola(EscolaEntity escola){
        Optional<EscolaEntity> optional = escolaService.findByEscolaPorId(escola.getId());
        if(optional.isEmpty()){
            throw new CadastroException("Escola com Nome  (" + escola.getPessoa().getNome()+") Não Cadastrado");
        }
        return optional.get();
    }

    private CidadeEntity validaCidade(CidadeEntity cidade){
        Optional<CidadeEntity> optional = cidadeService.findByCidadePorId(cidade.getId());
        if(optional.isEmpty()){
            throw new CadastroException("Cidade com Nome  (" + cidade.getNome()+") Não Cadastrado");
        }
        return optional.get();
    }

    private List<IrmaoEntity> validaIrmao(List<IrmaoEntity> irmaos){
        List<IrmaoEntity> irmaoEntityList = new ArrayList<>();
        for(int i=0; i< irmaos.size(); i++) {
            Optional<IrmaoEntity> optional = irmaoService.findByIrmaoPorId(irmaos.get(i).getId());
            if (optional.isEmpty()) {
                throw new CadastroException("Irmão com Nome  (" + irmaos.get(i).getPessoa().getNome()+") Não Cadastrado");
            } else {
                irmaoEntityList.add(optional.get());
            }
        }
        return irmaoEntityList;
    }

    private List<ProblemaSaudeEntity> validaProblemaSaude(List<ProblemaSaudeEntity> problemas){
        List<ProblemaSaudeEntity> problemaSaudeEntityList = new ArrayList<>();
        for(int i=0; i< problemas.size(); i++) {
            Optional<ProblemaSaudeEntity> optional = problemaSaudeService.findByProblemaSaudePorId(problemas.get(i).getId());
            if (optional.isEmpty()) {
                throw new CadastroException("Problema Saúde com Nome  (" + problemas.get(i).getNome()+") Não Cadastrado");
            } else {
                problemaSaudeEntityList.add(optional.get());
            }
        }
        return problemaSaudeEntityList;
    }

    private List<AlergiaEntity> validaAlergia(List<AlergiaEntity> alergias){
        List<AlergiaEntity> alergiaEntityList = new ArrayList<>();
        for(int i=0; i< alergias.size(); i++) {
            Optional<AlergiaEntity> optional = alergiaService.findByAlergiaPorId(alergias.get(i).getId());
            if (optional.isEmpty()) {
                throw new CadastroException("Alergia com Nome  (" + alergias.get(i).getNome()+") Não Cadastrado");
            } else {
                alergiaEntityList.add(optional.get());
            }
        }
        return alergiaEntityList;
    }

    private RegistroEntity cadastrarRegistro(RegistroEntity registro){ return registroService.salvar(registro);
      }

    private PessoaFisicaEntity cadastrarPessoa(PessoaFisicaEntity pessoa){
        PessoaFisicaEntity pessoaFisica = pessoaFisicaService.salvar(pessoa);
        Optional<AlunoEntity> optional = alunoRepository.findByAluno(pessoaFisica.getId());
        if(optional.isPresent()){
            throw new CadastroException("Aluno com ID  (" + optional.get().getId()+") Já Existe");
        }
        return pessoaFisica;
    }


    /////////////////// VERIFICA ID DOS ALUNOS//////////////////////////////
    @Transactional
    public boolean existsById(Long registroId) {
        return alunoRepository.existsById(registroId);
    }


    ///////////////////////EXCLUI OS ALUNOS/////////////////////////////////
    @Transactional
    public boolean excluir(Long alunoId) {
        Optional<RegistroEntity> optional = alunoRepository.findByRegistro(alunoId);
        alunoRepository.deleteById(alunoId);
        registroService.excluir(optional.get().getId());
        return true;
    }
}
