package petter.associacao.domain.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import petter.associacao.domain.model.PessoaEntity;
import petter.associacao.domain.model.PessoaFisicaEntity;
import petter.associacao.domain.repository.PessoaRepository;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class PessoaService {

    private PessoaRepository pessoaRepository;



    //////////////////LISTA TODAS AS PESSOAS////////////////////////////////

    @Transactional
    public List<PessoaEntity> listar() {
        return pessoaRepository.findAll();
    }

///////////// BUSCA PESSOA FISICA POR ID/////////////////////////////

    @Transactional
    public Optional<PessoaEntity> findByPessoaPorId(Long pessoaNome) {
        return pessoaRepository.findById(pessoaNome);
    }

    //////////// BUSCA PESSOA FISICA POR NOME/////////////////////////////

    @Transactional
    public Optional<PessoaEntity> findByPessoaPorNome(String pessoaNome) {
        return pessoaRepository.findByNome(pessoaNome);
    }
}
