package petter.associacao.domain.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import petter.associacao.domain.exception.CadastroException;
import petter.associacao.domain.model.EnderecoEntity;
import petter.associacao.domain.model.PessoaFisicaEntity;
import petter.associacao.domain.repository.EnderecoRepository;
import petter.associacao.domain.repository.PessoaFisicaRepository;
import petter.associacao.domain.repository.PessoaRepository;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class PessoaFisicaService {

    private PessoaFisicaRepository pessoaFisicaRepository;
    private EnderecoService enderecoService;
    private PessoaRepository pessoaRepository;



    //////////////////LISTA TODAS AS PESSOAS////////////////////////////////

    @Transactional
    public List<PessoaFisicaEntity> listar() {
        return pessoaFisicaRepository.findAll();
    }

  ///////////// BUSCA PESSOA FISICA POR ID/////////////////////////////
    @Transactional
    public Optional<PessoaFisicaEntity> findByPessoaFisicaPorId(Long pessoaFisicaId) {
        return pessoaFisicaRepository.findById(pessoaFisicaId);
    }

    //////////// BUSCA PESSOA FISICA POR NOME/////////////////////////////
    @Transactional
    public Optional<PessoaFisicaEntity> findByPessoaFisicaPorNome(String pessoaFisicaNome) {
        return pessoaFisicaRepository.findByNome(pessoaFisicaNome);
    }

    //////////// BUSCA PESSOA FISICA POR CPF/////////////////////////////
    @Transactional
    public Optional<PessoaFisicaEntity> findByPessoaFisicaPorCpf(String cpf) {
        return pessoaFisicaRepository.findByCpf(cpf);
    }

    ///////////////// SALVA AS PESSOA FISICA////////////////////////////////////////
    @Transactional
    public PessoaFisicaEntity salvar(PessoaFisicaEntity pessoa) {
        Optional<PessoaFisicaEntity> optional = this.findByPessoaFisicaPorCpf(pessoa.getCpf());
        //PessoaFisicaEntity pessoaFisica = new PessoaFisicaEntity();
        if (optional.isPresent()) {
           // pessoaFisica = optional.get();
            optional.get().setEndereco(this.validaEnderecoPessoaFisica(pessoa.getEndereco()));
            return optional.get();
        }
        pessoa.setEndereco(this.validaEnderecoPessoaFisica(pessoa.getEndereco()));
        return pessoaFisicaRepository.save(pessoa);
    }

    ///////////////////////ATUALIZA A PESSOA FISICA/////////////////////////
    @Transactional
    public PessoaFisicaEntity atualizar(PessoaFisicaEntity pessoa) {
        Optional<PessoaFisicaEntity> optional = this.findByPessoaFisicaPorId(pessoa.getId());
        pessoa.setId(optional.get().getId());
        pessoa.setNome(optional.get().getNome());
        pessoa.setCpf(optional.get().getCpf());
        pessoa.setRg(optional.get().getRg());
        pessoa.setDataNascimento(optional.get().getDataNascimento());
        pessoa.setEndereco(this.validaEnderecoPessoaFisica(pessoa.getEndereco()));
        return pessoaFisicaRepository.save(pessoa);

    }

    private EnderecoEntity validaEnderecoPessoaFisica(EnderecoEntity endereco){
        Optional<EnderecoEntity> optional = enderecoService.findByEnderecoPorId(endereco.getId());
        if(optional.isPresent()){
            endereco.setId(optional.get().getId());
            endereco = enderecoService.atualizar(endereco);
        } else {
            endereco = enderecoService.salvar(endereco);
        }
        return endereco;
    }

    ////////////////// VERIFICA ID PESSOA JURIDICA//////////////////////////////

    @Transactional
    public boolean existsById(Long pessoaFisicaNome) {
        return pessoaFisicaRepository.existsById(pessoaFisicaNome);
    }

///////////////////////EXCLUI PESSOA FISICA/////////////////////////////////

    @Transactional
    public boolean excluir(Long pessoaFisicaNome) {
        pessoaFisicaRepository.deleteById(pessoaFisicaNome);
        return true;
    }
}
