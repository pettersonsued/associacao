package petter.associacao.domain.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import petter.associacao.domain.exception.CadastroException;
import petter.associacao.domain.model.*;
import petter.associacao.domain.repository.EnderecoRepository;
import petter.associacao.domain.repository.MaeRepository;
import petter.associacao.domain.repository.PessoaFisicaRepository;
import petter.associacao.domain.repository.PessoaRepository;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class MaeService {


    private MaeRepository maeRepository;

    private EnderecoRepository enderecoRepository;

    private PessoaFisicaService pessoaFisicaService;





    //////////////////LISTA TODOS AS MÃES////////////////////////////////

    @Transactional
    public List<MaeEntity> listar() {
        return maeRepository.findAll();
    }

///////////// BUSCA MÃES POR ID/////////////////////////////

    @Transactional
    public Optional<MaeEntity> findByMaePorId(Long maeId) {
        return maeRepository.findById(maeId);
    }

    //////////// BUSCA MÃES POR NOME/////////////////////////////

    @Transactional
    public Optional<MaeEntity> findByMaePorNome(String nome) {
        return maeRepository.findByNome(nome);
    }

    ///////////////// SALVA AS MÃE////////////////////////////////////////

    @Transactional
    public MaeEntity salvar(MaeEntity mae) {
        Optional<MaeEntity> optional = maeRepository.findByNome(mae.getPessoa().getNome());
        if (optional.isPresent()) {
            throw new CadastroException("Já existe Mãe com esse Nome  " + optional.get().getPessoa().getNome());
        }
        //mae.getPessoa().setEndereco(this.validaEnderecoMae(mae));
         Optional<PessoaFisicaEntity> optoinal1 = pessoaFisicaService.findByPessoaFisicaPorNome(mae.getPessoa().getNome());
         if(optoinal1.isEmpty()){
             PessoaFisicaEntity pessoaFisica = pessoaFisicaService.salvar(mae.getPessoa());
             mae.setPessoa(pessoaFisica);
         } else {
             mae.setPessoa(optoinal1.get());
         }
        return maeRepository.save(mae);
    }

    ///////////////////////ATUALIZA A MÃE/////////////////////////

    @Transactional
    public MaeEntity atualizar(MaeEntity mae) {
        Optional<MaeEntity> optional = this.findByMaePorId(mae.getId());
        mae.setId(optional.get().getId());
        Optional<PessoaFisicaEntity> pessoaFisica = pessoaFisicaService.findByPessoaFisicaPorNome(mae.getPessoa().getNome());
        if(pessoaFisica.isEmpty()){
            throw new CadastroException("Mãe com esse Nome  (" + optional.get().getPessoa().getNome()+ ") Não encontrado "+
                    "/ Não é possível atualizar nome do Mae");
        }
        mae.getPessoa().setId(pessoaFisica.get().getId());
        mae.setPessoa(pessoaFisicaService.atualizar(mae.getPessoa()));
        return maeRepository.save(mae);
    }

    /*private EnderecoEntity validaEnderecoMae(MaeEntity mae){
        Optional<EnderecoEntity> optional = enderecoRepository.findById(mae.getPessoa().getEndereco().getId());
        if(optional.isEmpty()){
            throw new CadastroException("Endereço com ID  (" + mae.getPessoa().getEndereco().getId() + ") Não existe");
        }
        EnderecoEntity endereco = optional.get();
        return endereco;
    }*/

    /////////////////// VERIFICA ID PESSOA JURIDICA//////////////////////////////

    @Transactional
    public boolean existsById(Long maeId) {
        return maeRepository.existsById(maeId);
    }

///////////////////////EXCLUI PESSOA JURIDICA/////////////////////////////////

    @Transactional
    public boolean excluir(Long maeId) {
        maeRepository.deleteById(maeId);
        return true;
    }
}
