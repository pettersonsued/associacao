package petter.associacao.domain.service;


import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import petter.associacao.domain.exception.CadastroException;
import petter.associacao.domain.model.AlergiaEntity;
import petter.associacao.domain.model.ProblemaSaudeEntity;
import petter.associacao.domain.repository.ProblemaSaudeRepository;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class ProblemaSaudeService {

    private ProblemaSaudeRepository problemaSaudeRepository;




    //////////////////LISTA TODOS PROBLEMA DE SAÚDE///////////////////////////////

    @Transactional
    public List<ProblemaSaudeEntity> listar() {
        return problemaSaudeRepository.findAll();
    }

///////////// BUSCA PROBLEMA DE SAÚDE POR ID/////////////////////////////

    @Transactional
    public Optional<ProblemaSaudeEntity> findByProblemaSaudePorId(Long problemaId) {
        return problemaSaudeRepository.findById(problemaId);
    }

    //////////// BUSCA PROBLEMA DE SAÚDE POR NOME/////////////////////////////

    @Transactional
    public Optional<ProblemaSaudeEntity> findByProblemaSaudePorNome(String nome) {
        return problemaSaudeRepository.findByNome(nome);
    }

    ///////////////// SALVA PROBLEMA DE SAÚDE////////////////////////////////////////

    @Transactional
    public ProblemaSaudeEntity salvar(ProblemaSaudeEntity problema) {
        Optional<ProblemaSaudeEntity> optional = problemaSaudeRepository.findByNomeCausa(problema.getNome(), problema.getCausa());
        if (optional.isPresent()) {
            throw new CadastroException("Já existe Problema de Saúde com esse Nome (" + optional.get().getNome()+") E com essa Causa ("+
                    optional.get().getCausa()+" ) / Sugiro mudar a Causa, ou Nome da Alergia");
        }
        return problemaSaudeRepository.save(problema);
    }

    ///////////////////////ATUALIZA PROBLEMA DE SAÚDE/////////////////////////

    @Transactional
    public ProblemaSaudeEntity atualizar(ProblemaSaudeEntity problema) {
        Optional<ProblemaSaudeEntity> optional = this.findByProblemaSaudePorId(problema.getId());
        ProblemaSaudeEntity problemaEntity = optional.get();
        problema.setId(problemaEntity.getId());
        problema.setNome(problemaEntity.getNome());
        problema.setCausa(problemaEntity.getCausa());
        return problemaSaudeRepository.save(problema);
    }

    /////////////////// VERIFICA ID PROBLEMA DE SAÚDE//////////////////////////////

    @Transactional
    public boolean existsById(Long problemaId) {
        return problemaSaudeRepository.existsById(problemaId);
    }

///////////////////////EXCLUI PROBLEMA DE SAÚDE/////////////////////////////////

    @Transactional
    public boolean excluir(Long problemaId) {
        problemaSaudeRepository.deleteById(problemaId);
        return true;
    }
}
