package petter.associacao.domain.service;


import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import petter.associacao.domain.exception.CadastroException;
import petter.associacao.domain.model.*;
import petter.associacao.domain.repository.EnderecoRepository;
import petter.associacao.domain.repository.EscolaRepository;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class EscolaService {

    private EscolaRepository escolaRepository;

    //private EnderecoRepository enderecoRepository;

    private PessoaJuridicaService pessoaJuridicaService;




    //////////////////LISTA TODOS AS ESCOLA///////////////////////////////

    @Transactional
    public List<EscolaEntity> listar() {
        return escolaRepository.findAll();
    }

///////////// BUSCA ESCOLA POR ID/////////////////////////////

    @Transactional
    public Optional<EscolaEntity> findByEscolaPorId(Long escolaId) {
        return escolaRepository.findById(escolaId);
    }

    //////////// BUSCA ESCOLA POR NOME/////////////////////////////

    @Transactional
    public Optional<EscolaEntity> findByEscolaPorNome(String nome) {
        return escolaRepository.findByNome(nome);
    }

    ///////////////// SALVA AS ESCOLA////////////////////////////////////////

    @Transactional
    public EscolaEntity salvar(EscolaEntity escola) {
        Optional<EscolaEntity> optional = escolaRepository.findByNome(escola.getPessoa().getNome());
        if (optional.isPresent()) {
            throw new CadastroException("Já existe Escola com esse Nome  " + optional.get().getPessoa().getNome());
        }
        Optional<PessoaJuridicaEntity> optoinal1 = pessoaJuridicaService.findByPessoaJuridicaPorNome(escola.getPessoa().getNome());
        if(optoinal1.isEmpty()){
            PessoaJuridicaEntity pessoaJuridica = pessoaJuridicaService.salvar(escola.getPessoa());
            escola.setPessoa(pessoaJuridica);
        } else {
            escola.setPessoa(optoinal1.get());
        }
        return escolaRepository.save(escola);
    }

    ///////////////////////ATUALIZA A ESCOLA/////////////////////////

    @Transactional
    public EscolaEntity atualizar(EscolaEntity escola) {
        Optional<EscolaEntity> optional = this.findByEscolaPorId(escola.getId());
        escola.setId(optional.get().getId());
        Optional<PessoaJuridicaEntity> optionalPessoaJuridica = pessoaJuridicaService.findByPessoaJuridicaPorNome(escola.getPessoa().getNome());
        if(optionalPessoaJuridica.isEmpty()){
            throw new CadastroException("Escola com esse Nome  (" + optional.get().getPessoa().getNome()+ ") Não encontrado "+
                    "/ Não é possível atualizar nome do Escola");
        }
        escola.getPessoa().setId(optionalPessoaJuridica.get().getId());
        escola.setPessoa(pessoaJuridicaService.atualizar(escola.getPessoa()));
        return escolaRepository.save(escola);
    }

    /////////////////// VERIFICA ID ESCOLA//////////////////////////////

    @Transactional
    public boolean existsById(Long escolaId) {
        return escolaRepository.existsById(escolaId);
    }

///////////////////////EXCLUI ESCOLA/////////////////////////////////

    @Transactional
    public boolean excluir(Long escolaId) {
        escolaRepository.deleteById(escolaId);
        return true;
    }
}
