package petter.associacao.domain.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import petter.associacao.domain.exception.CadastroException;
import petter.associacao.domain.model.CartorioEntity;
import petter.associacao.domain.model.DistritoEntity;
import petter.associacao.domain.model.RegistroEntity;
import petter.associacao.domain.model.RuaEntity;
import petter.associacao.domain.repository.CartorioRepository;
import petter.associacao.domain.repository.RegistroRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class RegistroService {

    private RegistroRepository registroRepository;

    //private CartorioRepository cartorioRepository;
    private CartorioService cartorioService;




    //////////// LISTA TODAS OS REGISTROS/////////////////////////////
    @Transactional
    public List<RegistroEntity> listar() {
        return registroRepository.findAll();
    }

    //////////// BUSCA  OS REGISTROS POR ID/////////////////////////////
    @Transactional
    public Optional<RegistroEntity> findByRegistroPorId(Long ruaId) {
        return registroRepository.findById(ruaId);
    }

    ///////////// BUSCA  OS REGISTROS POR NOME/////////////////////////////

   /* @Transactional
    public Optional<RegistroEntity> findByRuaPorNome(String cidadeNome) {
        return registroRepository.findByLogradouro(cidadeNome);
    }*/


    ///////////// SALVAR OS REGISTROS /////////////////////////////
    @Transactional
    public RegistroEntity salvar(RegistroEntity registro) {
        registro.setCartorio(this.validaCartorioExiste(registro.getCartorio()));
            Optional<RegistroEntity> optional = registroRepository.findBynumeroTermo(registro.getNumeroTermo());
            if (optional.isPresent()) {
                throw new CadastroException("Registro já exciste / Número do Termo (" + optional.get().getNumeroTermo() + ")");
            }
        return registroRepository.save(registro);
    }

    ////////////////////////ATUALIZA OS REGISTROS/////////////////////////

    /*@Transactional
    public RegistroEntity atualizar(RegistroEntity registro) {
        Optional<RegistroEntity> optional = this.findByRegistroPorId(registro.getId());
        registro.setId(optional.get().getId());
        registro.setNumeroLivro(optional.get().getNumeroLivro());
        registro.setNumeroFolha(optional.get().getNumeroFolha());
        registro.setNumeroTermo(optional.get().getNumeroTermo());
        registro.setCartorio(optional.get().getCartorio());
        return registroRepository.save(registro);
    }*/


    ///////////////// VERIFICA SE O CARTORIO EXISTE////////////////////////////////////////

    private CartorioEntity validaCartorioExiste(CartorioEntity cartorio){
        Optional<CartorioEntity> optional = cartorioService.findByCartorioPorId(cartorio.getId());
        if(optional.isEmpty()){
            throw new CadastroException("Cartório com ID (" +cartorio.getId()+ ") Não Existe");
        }
        CartorioEntity cartorioEntity = optional.get();
        return cartorioEntity;
    }

    /////////////////// VERIFICA ID OS REGISTROS//////////////////////////////
    @Transactional
    public boolean existsById(Long registroId) {
        return registroRepository.existsById(registroId);
    }


       ///////////////////////EXCLUI OS REGISTROS/////////////////////////////////
    @Transactional
    public boolean excluir(Long registroId) {
        registroRepository.deleteById(registroId);
        return true;
    }
}
