package petter.associacao.domain.service;


import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import petter.associacao.domain.exception.CadastroException;
import petter.associacao.domain.model.EnderecoEntity;
import petter.associacao.domain.model.IrmaoEntity;
import petter.associacao.domain.model.MaeEntity;
import petter.associacao.domain.model.PessoaFisicaEntity;
import petter.associacao.domain.repository.EnderecoRepository;
import petter.associacao.domain.repository.IrmaoRepository;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class IrmaoService {

    private IrmaoRepository irmaoRepository;

    private PessoaFisicaService pessoaFisicaService;

    private EnderecoRepository enderecoRepository;




    //////////////////LISTA TODOS OS IRMÃOS////////////////////////////////

    @Transactional
    public List<IrmaoEntity> listar() {
        return irmaoRepository.findAll();
    }

///////////// BUSCA  OS IRMÃOS POR ID/////////////////////////////

    @Transactional
    public Optional<IrmaoEntity> findByIrmaoPorId(Long irmaoId) {
        return irmaoRepository.findById(irmaoId);
    }

    //////////// BUSCA  OS IRMÃOS POR NOME/////////////////////////////

    @Transactional
    public Optional<IrmaoEntity> findByIrmaoPorNome(String nome) {
        return irmaoRepository.findByNome(nome);
    }

    ///////////////// SALVA OS IRMÃOS////////////////////////////////////////

    @Transactional
    public IrmaoEntity salvar(IrmaoEntity irmao) {
        Optional<IrmaoEntity> optional = irmaoRepository.findByNome(irmao.getPessoa().getNome());
        if (optional.isPresent()) {
            throw new CadastroException("Já existe Irmão com esse Nome  " + optional.get().getPessoa().getNome());
        }
        Optional<PessoaFisicaEntity> optoinal1 = pessoaFisicaService.findByPessoaFisicaPorNome(irmao.getPessoa().getNome());
        if(optoinal1.isEmpty()){
            PessoaFisicaEntity pessoaFisica = pessoaFisicaService.salvar(irmao.getPessoa());
            irmao.setPessoa(pessoaFisica);
        } else {
            irmao.setPessoa(optoinal1.get());
        }
        return irmaoRepository.save(irmao);
    }

    ///////////////////////ATUALIZA O IRMÃO////////////////////////

    @Transactional
    public IrmaoEntity atualizar(IrmaoEntity irmao) {
        Optional<IrmaoEntity> optional = this.findByIrmaoPorId(irmao.getId());
        irmao.setId(optional.get().getId());
        Optional<PessoaFisicaEntity> pessoaFisica = pessoaFisicaService.findByPessoaFisicaPorNome(irmao.getPessoa().getNome());
        if(pessoaFisica.isEmpty()){
            throw new CadastroException("Irmão com esse Nome  (" + optional.get().getPessoa().getNome()+ ") Não encontrado "+
                    "/ Não é possível atualizar nome do Mae");
        }
        irmao.getPessoa().setId(pessoaFisica.get().getId());
        irmao.setPessoa(pessoaFisicaService.atualizar(irmao.getPessoa()));
        return irmaoRepository.save(irmao);
    }

    /////////////////// VERIFICA ID IRMÃO//////////////////////////////

    @Transactional
    public boolean existsById(Long irmaoId) {
        return irmaoRepository.existsById(irmaoId);
    }

///////////////////////EXCLUI IRMÃO/////////////////////////////////

    @Transactional
    public boolean excluir(Long irmaoId) {
        irmaoRepository.deleteById(irmaoId);
        return true;
    }
}
