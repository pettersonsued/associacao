package petter.associacao.domain.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.AllArgsConstructor;
import petter.associacao.domain.exception.CadastroException;
import petter.associacao.domain.model.CidadeEntity;
import petter.associacao.domain.model.DistritoEntity;
import petter.associacao.domain.repository.CidadeRepository;
import petter.associacao.domain.repository.DistritoRepository;

@AllArgsConstructor
@Service
public class CidadeService {

	 @PersistenceContext
     private EntityManager em;
	 private CidadeRepository cidadeRepository;


//////////////////LISTA TODOS OS CIDADES////////////////////////////////

	@Transactional
	public List<CidadeEntity> listar() {
		return cidadeRepository.findAll();
	}

///////////// BUSCA CIDADES POR ID/////////////////////////////

	@Transactional
	public Optional<CidadeEntity> findByCidadePorId(Long cidadeId) {
		return cidadeRepository.findById(cidadeId);
	}

///////////// BUSCA CIDADES POR NOME/////////////////////////////

	@Transactional
	public Optional<CidadeEntity> findByCidadePorNome(String cidadeNome) {
		return cidadeRepository.findByNome(cidadeNome);
	}

///////////////// SALVA OS CIDADE////////////////////////////////////////

	@Transactional
	public CidadeEntity salvar(CidadeEntity cidade) {
		boolean nome = this.findByCidadePorNome(cidade.getNome()).stream()
				.anyMatch(cidadeExistente -> cidadeExistente.equals(cidade));
		if (nome) {
			throw new CadastroException("Já existe Cidade com esse Nome  " + cidade.getNome());
		}
		return cidadeRepository.save(cidade);
	}

////////////////////////ATUALIZA A CIDADE/////////////////////////

	@Transactional
	public CidadeEntity atualizar(CidadeEntity cidade) {
		return cidadeRepository.save(cidade);
		
	}

/////////////////// VERIFICA ID CIDADE//////////////////////////////

	@Transactional
	public boolean existsById(Long cidadeId) {
		return cidadeRepository.existsById(cidadeId);
	}

///////////////////////EXCLUI CIDADE/////////////////////////////////

	@Transactional
	public boolean excluir(Long cidadeId) {
		cidadeRepository.deleteById(cidadeId);
		return true;
	}

}
