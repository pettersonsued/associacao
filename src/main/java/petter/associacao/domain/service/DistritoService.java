package petter.associacao.domain.service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.AllArgsConstructor;
import petter.associacao.domain.exception.CadastroException;
import petter.associacao.domain.model.CidadeEntity;
import petter.associacao.domain.model.DistritoEntity;
import petter.associacao.domain.repository.CidadeRepository;
import petter.associacao.domain.repository.DistritoRepository;

@AllArgsConstructor
@Service
public class DistritoService {
	
	private DistritoRepository distritoRepository;
	//private CidadeService cidadeService;

	private CidadeRepository cidadeRepository;

	
	@Transactional
	public List<DistritoEntity> listar() {
		return distritoRepository.findAll();
	}
	
	@Transactional
	public Optional<DistritoEntity> findByDistritoPorId(Long distritoId) {
		return distritoRepository.findById(distritoId);
	}

	///////////// BUSCA DSTRITO POR NOME/////////////////////////////

	@Transactional
	public Optional<DistritoEntity> findByDistritoPorNome(String cidadeNome) {
		return distritoRepository.findByNome(cidadeNome);
	}

	///////////// CADASTRA DSTRITO /////////////////////////////
	@Transactional
	public DistritoEntity salvar(DistritoEntity distrito) {
		this.validaDistirtoQuandoVaiSalar(distrito);
	    List<DistritoEntity> listDistrito = new ArrayList<>();
		distrito.setCidade(this.validaCidadeQuandoSalvaDistrito(distrito.getCidade()));
		DistritoEntity dis = distritoRepository.save(distrito);
		listDistrito.add(dis);
		dis.getCidade().setDistritos(listDistrito);
		cidadeRepository.save(dis.getCidade());
		return dis;
	}

	///////////// aATUALIZA DSTRITO POR NOME/////////////////////////////
	@Transactional
	public DistritoEntity atualizar(DistritoEntity distrito) {
		this.validaDistirtoQuandoVaiSalar(distrito);
		return distritoRepository.save(distrito);
	}

	///////////// VALIDA DISTRITO/////////////////////////////
	private void validaDistirtoQuandoVaiSalar(DistritoEntity distrito){
		Optional<DistritoEntity> optionalDistrito = distritoRepository.findByDistrito(distrito.getNome() , distrito.getCidade().getId());
		if (optionalDistrito.isPresent()) {
			throw new CadastroException("Já existe Distrito com esse Nome  "+distrito.getNome());
		}
	}

	///////////// VALIDA CIDADE QUANDO SALVA O DISTRITO/////////////////////////////
	private CidadeEntity validaCidadeQuandoSalvaDistrito(CidadeEntity cidade){
		Optional<CidadeEntity> optional = cidadeRepository.findById(cidade.getId());
		if(optional.isEmpty()){
			throw new CadastroException("Cidade com ID (" +cidade.getId()+ ") não Existe");
		}
		CidadeEntity cidadeEntity = optional.get();
		return cidadeEntity;
	}


	/////////////////// VERIFICA ID DISTRITO//////////////////////////////

	@Transactional
	public boolean existsById(Long distritoId) {
		return distritoRepository.existsById(distritoId);
	}


	///////////////////////EXCLUI DISTRITO/////////////////////////////////

	@Transactional
	public boolean excluir(Long distritoId) {
		distritoRepository.deleteById(distritoId);
		return true;
	}

}
