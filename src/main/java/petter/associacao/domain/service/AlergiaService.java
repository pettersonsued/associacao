package petter.associacao.domain.service;


import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import petter.associacao.domain.exception.CadastroException;
import petter.associacao.domain.model.AlergiaEntity;
import petter.associacao.domain.model.MaeEntity;
import petter.associacao.domain.model.PessoaFisicaEntity;
import petter.associacao.domain.repository.AlergiaRepository;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class AlergiaService {

    private AlergiaRepository alergiaRepository;




    //////////////////LISTA TODOS AS ALERGIA///////////////////////////////

    @Transactional
    public List<AlergiaEntity> listar() {
        return alergiaRepository.findAll();
    }

///////////// BUSCA ALERGIA POR ID/////////////////////////////

    @Transactional
    public Optional<AlergiaEntity> findByAlergiaPorId(Long maeId) {
        return alergiaRepository.findById(maeId);
    }

    //////////// BUSCA ALERGIA POR NOME/////////////////////////////

    @Transactional
    public Optional<AlergiaEntity> findByAlergiaPorNome(String nome) {
        return alergiaRepository.findByNome(nome);
    }

    ///////////////// SALVA ALERGIA////////////////////////////////////////

    @Transactional
    public AlergiaEntity salvar(AlergiaEntity alergia) {
        Optional<AlergiaEntity> optional = alergiaRepository.findByNomeCausa(alergia.getNome(), alergia.getCausa());
        if (optional.isPresent()) {
            throw new CadastroException("Já existe Alergia com esse Nome (" + optional.get().getNome()+") E essa Causa ("+
                    optional.get().getCausa()+" ) / Sugiro mudar a Causa, ou Nome da Alergia");
        }
        return alergiaRepository.save(alergia);
    }

    ///////////////////////ATUALIZA ALERGIA/////////////////////////

    @Transactional
    public AlergiaEntity atualizar(AlergiaEntity alergia) {
        Optional<AlergiaEntity> optional = this.findByAlergiaPorId(alergia.getId());
        AlergiaEntity alergiaEntity = optional.get();
        alergia.setId(alergiaEntity.getId());
        alergia.setNome(alergiaEntity.getNome());
        alergia.setCausa(alergiaEntity.getCausa());
        return alergiaRepository.save(alergia);
    }

    /////////////////// VERIFICA ID ALERGIA//////////////////////////////

    @Transactional
    public boolean existsById(Long alergiaId) {
        return alergiaRepository.existsById(alergiaId);
    }

///////////////////////EXCLUI ALERGIA/////////////////////////////////

    @Transactional
    public boolean excluir(Long alergiaId) {
        alergiaRepository.deleteById(alergiaId);
        return true;
    }
}
