package petter.associacao.domain.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.AllArgsConstructor;
import petter.associacao.domain.exception.CadastroException;
import petter.associacao.domain.model.CidadeEntity;
import petter.associacao.domain.model.DistritoEntity;
import petter.associacao.domain.model.RuaEntity;
import petter.associacao.domain.repository.DistritoRepository;
import petter.associacao.domain.repository.RuaRepository;

@AllArgsConstructor
@Service
public class RuaService {

   private RuaRepository ruaRepository;

   private DistritoRepository distritoRepository;

	//////////// LISTA TODAS AS RUAS/////////////////////////////
	@Transactional
	public List<RuaEntity> listar() {
		return ruaRepository.findAll();
	}

	//////////// BUSCA RUA POR ID/////////////////////////////
	@Transactional
	public Optional<RuaEntity> findByRuaPorId(Long ruaId) {
		return ruaRepository.findById(ruaId);
	}

	///////////// BUSCA RUA POR NOME/////////////////////////////

	@Transactional
	public Optional<RuaEntity> findByRuaPorNome(String cidadeNome) {
		return ruaRepository.findByLogradouro(cidadeNome);
	}

	///////////// SALVAR RUA /////////////////////////////
	@Transactional
	public RuaEntity salvar(RuaEntity rua) {
		this.validarRuaQuandoVaiSalvar(rua);
		List<RuaEntity> listRua = new ArrayList<>();
		Optional<DistritoEntity> optional = distritoRepository.findById(rua.getDistrito().getId());
		rua.setDistrito(this.validaDistritoQuandoSalvaRua(rua.getDistrito()));
		RuaEntity ruaEntity = ruaRepository.save(rua);
		listRua.add(ruaEntity);
		ruaEntity.getDistrito().setRuas(listRua);
		distritoRepository.save(rua.getDistrito());
		return ruaEntity;
	}

	////////////////////////ATUALIZA A RUA/////////////////////////

	@Transactional
	public RuaEntity atualizar(RuaEntity rua) {
		this.validarRuaQuandoVaiSalvar(rua);
		return ruaRepository.save(rua);
	}

	///////////// VALIDA RUA/////////////////////////////
	private void validarRuaQuandoVaiSalvar(RuaEntity rua){
		Optional<RuaEntity> optionalRua = ruaRepository.findByRua(rua.getLogradouro(), rua.getDistrito().getId());
		if (optionalRua.isPresent()) {
			throw new CadastroException("Já existe Rua com esse Nome  ("+rua.getLogradouro()+") No Distrito ("+ rua.getDistrito().getNome()+")");
		}
	}

	///////////// VALIDA DISTRITO QUANDO SALVA A RUA/////////////////////////////
	private DistritoEntity validaDistritoQuandoSalvaRua(DistritoEntity distrito){
		Optional<DistritoEntity> optional = distritoRepository.findById(distrito.getId());
		if(optional.isEmpty()){
			throw new CadastroException("Distrito com ID (" +distrito.getId()+ ") não Existe");
		}
		DistritoEntity distritoEntity = optional.get();
		return distritoEntity;
	}


	/////////////////// VERIFICA ID RUA//////////////////////////////

	@Transactional
	public boolean existsById(Long ruaId) {
		return ruaRepository.existsById(ruaId);
	}


///////////////////////EXCLUI RUA/////////////////////////////////

	@Transactional
	public boolean excluir(Long ruaId) {
		ruaRepository.deleteById(ruaId);
		return true;
	}
}
