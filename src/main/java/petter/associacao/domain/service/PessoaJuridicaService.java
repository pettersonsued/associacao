package petter.associacao.domain.service;


import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import petter.associacao.domain.exception.CadastroException;
import petter.associacao.domain.model.EnderecoEntity;
import petter.associacao.domain.model.PessoaFisicaEntity;
import petter.associacao.domain.model.PessoaJuridicaEntity;
import petter.associacao.domain.repository.EnderecoRepository;
import petter.associacao.domain.repository.PessoaJuridicaRepository;
import petter.associacao.domain.repository.PessoaRepository;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class PessoaJuridicaService {

    private PessoaJuridicaRepository pessoaJuridicaRepository;

    private EnderecoService enderecoService;

    private PessoaService pessoaService;




    //////////////////LISTA TODOS OS PESSOAS////////////////////////////////

    @Transactional
    public List<PessoaJuridicaEntity> listar() {
        return pessoaJuridicaRepository.findAll();
    }

///////////// BUSCA PESSOA JURIDICA POR ID/////////////////////////////

    @Transactional
    public Optional<PessoaJuridicaEntity> findByPessoaJuridicaPorId(Long pessoaJuridicaId) {
        return pessoaJuridicaRepository.findById(pessoaJuridicaId);
    }

    //////////// BUSCA PESSOA JURIDICA POR NOME/////////////////////////////

    @Transactional
    public Optional<PessoaJuridicaEntity> findByPessoaJuridicaPorNome(String pessoaJuridicaNome) {
        return pessoaJuridicaRepository.findByNome(pessoaJuridicaNome);
    }

///////////////// SALVA AS PESSOA JURIDICA////////////////////////////////////////

    @Transactional
    public PessoaJuridicaEntity salvar(PessoaJuridicaEntity pessoa) {
        boolean nome = pessoaService.findByPessoaPorNome(pessoa.getNome()).stream()
                .anyMatch(pessoaExiste -> pessoaExiste.getNome().equals(pessoa.getNome()));
        if (nome) {
            throw new CadastroException("Já existe Estabelecimento com esse Nome  " + pessoa.getNome());
        }
        pessoa.setEndereco(this.validaEnderecoPessoaJuridica(pessoa.getEndereco()));
        return pessoaJuridicaRepository.save(pessoa);
    }

    ///////////////////////ATUALIZA A PESSOA JURIDICA/////////////////////////

    @Transactional
    public PessoaJuridicaEntity atualizar(PessoaJuridicaEntity pessoa) {
        Optional<PessoaJuridicaEntity> optional = this.findByPessoaJuridicaPorId(pessoa.getId());
        pessoa.setId(optional.get().getId());
        pessoa.setNome(optional.get().getNome());
        pessoa.setCnpj(optional.get().getCnpj());
        pessoa.setRazao(optional.get().getRazao());
        pessoa.setDataFundacao(optional.get().getDataFundacao());
        pessoa.setEndereco(this.validaEnderecoPessoaJuridica(pessoa.getEndereco()));
        return pessoaJuridicaRepository.save(pessoa);
    }

    private EnderecoEntity validaEnderecoPessoaJuridica(EnderecoEntity endereco){
        Optional<EnderecoEntity> optional = enderecoService.findByEnderecoPorId(endereco.getId());
        if(optional.isPresent()){
            endereco.setId(optional.get().getId());
            endereco = enderecoService.atualizar(endereco);
        } else {
             endereco = enderecoService.salvar(endereco);
        }
        return endereco;
    }

/////////////////// VERIFICA ID PESSOA JURIDICA//////////////////////////////

    @Transactional
    public boolean existsById(Long pessoaJuridicaId) {
        return pessoaJuridicaRepository.existsById(pessoaJuridicaId);
    }

///////////////////////EXCLUI PESSOA JURIDICA/////////////////////////////////

    @Transactional
    public boolean excluir(Long pessoaJuridicaId) {
        pessoaJuridicaRepository.deleteById(pessoaJuridicaId);
        return true;
    }
}
