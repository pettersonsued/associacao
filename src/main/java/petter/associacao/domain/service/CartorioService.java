package petter.associacao.domain.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import petter.associacao.domain.exception.CadastroException;
import petter.associacao.domain.model.*;
import petter.associacao.domain.repository.CartorioRepository;
import petter.associacao.domain.repository.EnderecoRepository;
import petter.associacao.domain.repository.PessoaJuridicaRepository;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class CartorioService {

     private CartorioRepository cartorioRepository;

     private EnderecoRepository enderecoRepository;

     private PessoaJuridicaService pessoaJuridicaService;



     //////////////////LISTA TODOS OS CARTORIOS////////////////////////////////

     @Transactional
     public List<CartorioEntity> listar() {
          return cartorioRepository.findAll();
     }

///////////// BUSCA OS CARTORIOS POR ID/////////////////////////////

     @Transactional
     public Optional<CartorioEntity> findByCartorioPorId(Long cartorioId) {
          return cartorioRepository.findById(cartorioId);
     }

     //////////// BUSCA OS CARTORIOS POR NOME/////////////////////////////

     @Transactional
     public Optional<CartorioEntity> findByCartorioPorNome(String nome) {
          return cartorioRepository.findByNome(nome);
     }

     ///////////////// SALVA AS MÃE////////////////////////////////////////

     @Transactional
     public CartorioEntity salvar(CartorioEntity cartorio) {
          Optional<CartorioEntity> optional = cartorioRepository.findByNome(cartorio.getPessoa().getNome());
          if (optional.isPresent()) {
               throw new CadastroException("Já existe Cartório com esse Nome  " + optional.get().getPessoa().getNome());
          }
          Optional<PessoaJuridicaEntity> optoinal1 = pessoaJuridicaService.findByPessoaJuridicaPorNome(cartorio.getPessoa().getNome());
          if(optoinal1.isEmpty()){
               PessoaJuridicaEntity pessoaFisica = pessoaJuridicaService.salvar(cartorio.getPessoa());
               cartorio.setPessoa(pessoaFisica);
          } else {
               cartorio.setPessoa(optoinal1.get());
          }
          return cartorioRepository.save(cartorio);
     }

     ///////////////////////ATUALIZA CARTORIO/////////////////////////

     @Transactional
     public CartorioEntity atualizar(CartorioEntity cartorio) {
          Optional<CartorioEntity> optional = this.findByCartorioPorId(cartorio.getId());
          cartorio.setId(optional.get().getId());
          Optional<PessoaJuridicaEntity> pessoaJuridica = pessoaJuridicaService.findByPessoaJuridicaPorNome(cartorio.getPessoa().getNome());
          if(pessoaJuridica.isEmpty()){
               throw new CadastroException("Cartório com esse Nome  (" + cartorio.getPessoa().getNome()+ ") Não encontrado "+
                       "/ Não é possível atualizar nome do Cartório");
          }
          cartorio.getPessoa().setId(pessoaJuridica.get().getId());
          cartorio.setPessoa(pessoaJuridicaService.atualizar(cartorio.getPessoa()));
          return cartorioRepository.save(cartorio);
     }

     /////////////////// VERIFICA ID CARTORIO//////////////////////////////

     @Transactional
     public boolean existsById(Long cartorioId) {
          return cartorioRepository.existsById(cartorioId);
     }

///////////////////////EXCLUI CARTORIO/////////////////////////////////

     @Transactional
     public boolean excluir(Long cartorioId) {
          cartorioRepository.deleteById(cartorioId);
          return true;
     }
}
