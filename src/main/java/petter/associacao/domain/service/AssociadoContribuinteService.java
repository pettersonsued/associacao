package petter.associacao.domain.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import petter.associacao.domain.exception.CadastroException;
import petter.associacao.domain.model.AssociadoContribuinteEntity;
import petter.associacao.domain.model.EnderecoEntity;
import petter.associacao.domain.model.MaeEntity;
import petter.associacao.domain.model.PessoaFisicaEntity;
import petter.associacao.domain.repository.*;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class AssociadoContribuinteService {

    private AssociadoContribuinteRepository associadoContribuinteRepository;
    private EnderecoRepository enderecoRepository;
    private PessoaFisicaService pessoaFisicaService;



    //////////////////LISTA TODOS OS ASSOCIADOS CONTRIBUINTE////////////////////////////////

    @Transactional
    public List<AssociadoContribuinteEntity> listar() {
        return associadoContribuinteRepository.findAll();
    }

///////////// BUSCA ASSOCIADO CONTRIBUINTE POR ID/////////////////////////////

    @Transactional
    public Optional<AssociadoContribuinteEntity> findByContribuintePorId(Long contribuinteId) {
        return associadoContribuinteRepository.findById(contribuinteId);
    }

    //////////// BUSCA ASSOCIADO CONTRIBUINTE POR NOME/////////////////////////////

    @Transactional
    public Optional<AssociadoContribuinteEntity> findByContribuintePorNome(String nome) {
        return associadoContribuinteRepository.findByNome(nome);
    }

    /////////////////// SALVA ASSOCIADO CONTRIBUINTE//////////////////////////////

    @Transactional
    public AssociadoContribuinteEntity salvar(AssociadoContribuinteEntity contribuinte) {
        Optional<AssociadoContribuinteEntity> optional = associadoContribuinteRepository.findByNome(contribuinte.getPessoa().getNome());
        if (optional.isPresent()) {
            throw new CadastroException("Já existe Contribuinte com esse Nome  " + optional.get().getPessoa().getNome());
        }
        contribuinte.getPessoa().setEndereco(this.validaEnderecoContribuinte(contribuinte));
        Optional<PessoaFisicaEntity> optoinal1 = pessoaFisicaService.findByPessoaFisicaPorNome(contribuinte.getPessoa().getNome());
        if(optoinal1.isEmpty()){
            PessoaFisicaEntity pessoaFisica = pessoaFisicaService.salvar(contribuinte.getPessoa());
            contribuinte.setPessoa(pessoaFisica);
        } else {
            contribuinte.setPessoa(optoinal1.get());
        }
        return associadoContribuinteRepository.save(contribuinte);
    }

    ///////////////////////ATUALIZA A ASSOCIADO CONTRIBUINTE/////////////////////////

    @Transactional
    public AssociadoContribuinteEntity atualizar(AssociadoContribuinteEntity contribuinte) {
        Optional<AssociadoContribuinteEntity> optional = this.findByContribuintePorId(contribuinte.getId());
        AssociadoContribuinteEntity pessoaEntity = optional.get();
        contribuinte.setId(pessoaEntity.getId());
        Optional<PessoaFisicaEntity> pessoaFisica = pessoaFisicaService.findByPessoaFisicaPorNome(contribuinte.getPessoa().getNome());
        contribuinte.getPessoa().setId(pessoaFisica.get().getId());
        contribuinte.setPessoa(pessoaFisicaService.atualizar(contribuinte.getPessoa()));
        return associadoContribuinteRepository.save(contribuinte);
    }

    /////////////////// VALIDA ASSOCIADO CONTRIBUINTE//////////////////////////////
    private EnderecoEntity validaEnderecoContribuinte(AssociadoContribuinteEntity contribuinte){
        Optional<EnderecoEntity> optional = enderecoRepository.findById(contribuinte.getPessoa().getEndereco().getId());
        if(optional.isEmpty()){
            throw new CadastroException("Endereço com ID  (" + contribuinte.getPessoa().getEndereco().getId() + ") Não existe");
        }
        EnderecoEntity endereco = optional.get();
        return endereco;
    }

    /////////////////// VERIFICA ID ASSOCIADO CONTRIBUINTE//////////////////////////////

    @Transactional
    public boolean existsById(Long contribuinteId) {
        return associadoContribuinteRepository.existsById(contribuinteId);
    }

///////////////////////EXCLUI ASSOCIADO CONTRIBUINTE/////////////////////////////////

    @Transactional
    public boolean excluir(Long contribuinteId) {
        associadoContribuinteRepository.deleteById(contribuinteId);
        return true;
    }
}
