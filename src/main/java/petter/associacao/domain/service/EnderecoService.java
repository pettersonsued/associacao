package petter.associacao.domain.service;


import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import petter.associacao.domain.exception.CadastroException;
import petter.associacao.domain.model.*;
import petter.associacao.domain.repository.EnderecoRepository;
import petter.associacao.domain.repository.RuaRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class EnderecoService {



    private EnderecoRepository enderecoRepository;

    private RuaRepository ruaRepository;


    //////////////////LISTA TODOS OS ENDERECOS////////////////////////////////
    @Transactional
    public List<EnderecoEntity> listar() {
        return enderecoRepository.findAll();
    }

    ///////////// BUSCA ENDEREÇO POR ID/////////////////////////////
    @Transactional
    public Optional<EnderecoEntity> findByEnderecoPorId(Long enderecoId) {
        return enderecoRepository.findById(enderecoId);
    }

    ///////////// SALVA UM ENDEREÇO/////////////////////////////
    @Transactional
    public EnderecoEntity salvar(EnderecoEntity endereco) {
        endereco.setRua(this.validaRuaQuandoSalvaEndereco(endereco.getRua()));
        return enderecoRepository.save(endereco);
    }

    ///////////// ATUALIZA ENDEREÇO/////////////////////////////
    @Transactional
    public EnderecoEntity atualizar(EnderecoEntity endereco) {
        this.validaRuaQuandoSalvaEndereco(endereco.getRua());
        return enderecoRepository.save(endereco);
    }


    ///////////// VALIDA RUA QUANDO SALVA ENDEREÇO/////////////////////////////
    private RuaEntity validaRuaQuandoSalvaEndereco(RuaEntity rua){
        Optional<RuaEntity> optional = ruaRepository.findById(rua.getId());
        if(optional.isEmpty()){
            throw new CadastroException("Rua com ID (" +rua.getId()+ ") não Existe");
        }
        RuaEntity ruaEntity = optional.get();
        return ruaEntity;
    }

/////////////////// VERIFICA ID ENDERECOS//////////////////////////////

    @Transactional
    public boolean existsById(Long enderecoId) {
        return enderecoRepository.existsById(enderecoId);
    }

///////////////////////EXCLUI ENDERECOS/////////////////////////////////

    @Transactional
    public boolean excluir(Long enderecod) {
        enderecoRepository.deleteById(enderecod);
        return true;
    }

}
