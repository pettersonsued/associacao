package petter.associacao.domain.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import petter.associacao.domain.exception.CadastroException;
import petter.associacao.domain.model.EnderecoEntity;
import petter.associacao.domain.model.MaeEntity;
import petter.associacao.domain.model.PaiEntity;
import petter.associacao.domain.model.PessoaFisicaEntity;
import petter.associacao.domain.repository.EnderecoRepository;
import petter.associacao.domain.repository.PaiRepository;
import petter.associacao.domain.repository.PessoaRepository;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class PaiService {

    private PaiRepository paiRepository;

    private EnderecoRepository enderecoRepository;

    private PessoaFisicaService pessoaFisicaService;


    //////////////////LISTA TODOS OS PAIS////////////////////////////////
    @Transactional
    public List<PaiEntity> listar() {
        return paiRepository.findAll();
    }

///////////// BUSCA OS PAIS POR ID/////////////////////////////

    @Transactional
    public Optional<PaiEntity> findByPaiPorId(Long paiId) {
        return paiRepository.findById(paiId);
    }

    //////////// BUSCA OS PAIS POR NOME/////////////////////////////

    @Transactional
    public Optional<PaiEntity> findByPaiPorNome(String nome) {
        return paiRepository.findByNome(nome);
    }

    ///////////////// SALVA OS PAIS////////////////////////////////////////

    @Transactional
    public PaiEntity salvar(PaiEntity pai) {
        Optional<PaiEntity> optional = paiRepository.findByNome(pai.getPessoa().getNome());
        if (optional.isPresent()) {
            throw new CadastroException("Já existe Pai com esse Nome  " + optional.get().getPessoa().getNome());
        }
        Optional<PessoaFisicaEntity> optoinal1 = pessoaFisicaService.findByPessoaFisicaPorNome(pai.getPessoa().getNome());
        if(optoinal1.isEmpty()){
            PessoaFisicaEntity pessoaFisica = pessoaFisicaService.salvar(pai.getPessoa());
            pai.setPessoa(pessoaFisica);
        } else {
            pai.setPessoa(optoinal1.get());
        }
        return paiRepository.save(pai);
    }

    ///////////////////////ATUALIZA O PAI/////////////////////////

    @Transactional
    public PaiEntity atualizar(PaiEntity pai) {
        Optional<PaiEntity> optional = this.findByPaiPorId(pai.getId());
        pai.setId(optional.get().getId());
        Optional<PessoaFisicaEntity> pessoaFisica = pessoaFisicaService.findByPessoaFisicaPorNome(pai.getPessoa().getNome());
        if(pessoaFisica.isEmpty()){
            throw new CadastroException("Pai com esse Nome  (" + optional.get().getPessoa().getNome()+ ") Não encontrado "+
                    "/ Não é possível atualizar nome do Pai");
        }
        pai.getPessoa().setId(pessoaFisica.get().getId());
        pai.setPessoa(pessoaFisicaService.atualizar(pai.getPessoa()));
        return paiRepository.save(pai);
    }

    /////////////////// VERIFICA ID PAI//////////////////////////////

    @Transactional
    public boolean existsById(Long paiId) {
        return paiRepository.existsById(paiId);
    }

///////////////////////EXCLUI PAI/////////////////////////////////

    @Transactional
    public boolean excluir(Long paiId) {
        paiRepository.deleteById(paiId);
        return true;
    }
}
