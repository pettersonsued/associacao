package petter.associacao.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import petter.associacao.domain.model.CartorioEntity;

import java.util.Optional;

@Repository
public interface CartorioRepository extends JpaRepository<CartorioEntity, Long> {

    @Query("select c from CartorioEntity c inner join c.pessoa p where p.nome = :nome")
    Optional<CartorioEntity> findByNome(String nome);

}
