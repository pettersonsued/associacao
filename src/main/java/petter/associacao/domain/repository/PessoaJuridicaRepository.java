package petter.associacao.domain.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import petter.associacao.domain.model.PessoaJuridicaEntity;

import java.util.Optional;

@Repository
public interface PessoaJuridicaRepository extends JpaRepository<PessoaJuridicaEntity, Long> {
    Optional<PessoaJuridicaEntity> findByNome(String nome);
}
