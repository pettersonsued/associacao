package petter.associacao.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import petter.associacao.domain.model.IrmaoEntity;
import petter.associacao.domain.model.MaeEntity;

import java.util.Optional;

@Repository
public interface IrmaoRepository extends JpaRepository<IrmaoEntity, Long> {

    @Query("select i from IrmaoEntity i inner join i.pessoa p where p.nome = :nome")
    Optional<IrmaoEntity> findByNome(String nome);
}
