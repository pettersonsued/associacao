package petter.associacao.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import petter.associacao.domain.model.AssociadoContribuinteEntity;
import petter.associacao.domain.model.MaeEntity;
import petter.associacao.domain.model.PessoaEntity;

import java.util.Optional;

@Repository
public interface AssociadoContribuinteRepository extends JpaRepository<AssociadoContribuinteEntity, Long> {

    @Query("select a from AssociadoContribuinteEntity a inner join a.pessoa p where p.nome = :nome")
    Optional<AssociadoContribuinteEntity> findByNome(String nome);

    //Optional<AssociadoContribuinteEntity> findByPessoaFisicaNome(String nome);
}
