package petter.associacao.domain.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import petter.associacao.domain.model.CidadeEntity;
import petter.associacao.domain.model.DistritoEntity;

@Repository
public interface DistritoRepository extends JpaRepository<DistritoEntity, Long>{
	@Query("select d from DistritoEntity d inner join d.cidade c where d.nome = :nome AND c.id = :cidadeId")
	Optional<DistritoEntity> findByDistrito(String nome, Long cidadeId);

	Optional<DistritoEntity> findByNome(String nome);
}
