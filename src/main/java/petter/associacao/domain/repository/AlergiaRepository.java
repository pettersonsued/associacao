package petter.associacao.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import petter.associacao.domain.model.AlergiaEntity;
import petter.associacao.domain.model.PaiEntity;

import java.util.Optional;

@Repository
public interface AlergiaRepository extends JpaRepository<AlergiaEntity, Long> {

    @Query("select a from AlergiaEntity a where a.nome = :nome AND a.causa =:causa")
    Optional<AlergiaEntity> findByNomeCausa(String nome, String causa);

    Optional<AlergiaEntity> findByNome(String nome);
}
