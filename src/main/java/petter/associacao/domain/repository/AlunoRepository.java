package petter.associacao.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import petter.associacao.domain.model.AlunoEntity;
import petter.associacao.domain.model.RegistroEntity;

import java.util.Optional;
@Repository
public interface AlunoRepository extends JpaRepository<AlunoEntity, Long> {

    @Query("select a from AlunoEntity a inner join a.pessoa p where p.nome = :nome")
    Optional<AlunoEntity> findByNome(String nome);

    @Query("select a from AlunoEntity a inner join a.pessoa p where p.id = :pessoaId")
    Optional<AlunoEntity> findByAluno(Long pessoaId);

    @Query("select a from AlunoEntity a inner join a.pessoa p where p.cpf = :cpf")
    Optional<AlunoEntity> findByAlunoPorCpf(String cpf);

   @Query("select r from AlunoEntity a inner join a.registro r where a.id = :alunoId")
    Optional<RegistroEntity> findByRegistro(Long alunoId);
}
