package petter.associacao.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import petter.associacao.domain.model.ProblemaSaudeEntity;

import java.util.Optional;

@Repository
public interface ProblemaSaudeRepository extends JpaRepository<ProblemaSaudeEntity, Long> {

    @Query("select p from ProblemaSaudeEntity p where p.nome = :nome AND p.causa =:causa")
    Optional<ProblemaSaudeEntity> findByNomeCausa(String nome, String causa);

    Optional<ProblemaSaudeEntity> findByNome(String nome);
}
