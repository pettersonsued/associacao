package petter.associacao.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import petter.associacao.domain.model.PessoaEntity;

import java.util.Optional;

@Repository
public interface PessoaRepository extends JpaRepository<PessoaEntity, Long> {

    Optional<PessoaEntity> findByNome(String nome);
}
