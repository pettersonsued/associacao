package petter.associacao.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import petter.associacao.domain.model.PaiEntity;

import java.util.Optional;

@Repository
public interface PaiRepository extends JpaRepository<PaiEntity, Long> {

    @Query("select p from PaiEntity p inner join p.pessoa s where s.nome = :nome")
    Optional<PaiEntity> findByNome(String nome);
}
