package petter.associacao.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import petter.associacao.domain.model.EscolaEntity;
import petter.associacao.domain.model.MaeEntity;

import java.util.Optional;

@Repository
public interface EscolaRepository extends JpaRepository<EscolaEntity, Long> {

    @Query("select e from EscolaEntity e inner join e.pessoa p where p.nome = :nome")
    Optional<EscolaEntity> findByNome(String nome);
}
