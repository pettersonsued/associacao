package petter.associacao.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import petter.associacao.domain.model.DistritoEntity;
import petter.associacao.domain.model.MaeEntity;
import petter.associacao.domain.model.PessoaFisicaEntity;

import java.util.Optional;

@Repository
public interface MaeRepository extends JpaRepository<MaeEntity, Long> {

    @Query("select m from MaeEntity m inner join m.pessoa p where p.nome = :nome")
    Optional<MaeEntity> findByNome(String nome);
}
