package petter.associacao.domain.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import petter.associacao.domain.model.DistritoEntity;
import petter.associacao.domain.model.RuaEntity;

@Repository
public interface RuaRepository extends JpaRepository<RuaEntity, Long>{

	@Query("select r from RuaEntity r inner join r.distrito d where r.logradouro = :logradouro AND d.id = :distritoId")
	Optional<RuaEntity> findByRua(String logradouro, Long distritoId);
	Optional<RuaEntity> findByLogradouro(String logradouro);
}
