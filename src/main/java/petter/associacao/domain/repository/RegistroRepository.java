package petter.associacao.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import petter.associacao.domain.model.MaeEntity;
import petter.associacao.domain.model.RegistroEntity;

import java.util.Optional;

@Repository
public interface RegistroRepository extends JpaRepository<RegistroEntity, Long> {

    //@Query("select m from MaeEntity m inner join m.pessoa p where p.nome = :nome")
    Optional<RegistroEntity> findBynumeroTermo(Integer numeroTermo);
}
