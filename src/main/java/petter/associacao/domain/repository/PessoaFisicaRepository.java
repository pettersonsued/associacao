package petter.associacao.domain.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import petter.associacao.domain.model.PessoaFisicaEntity;

import java.util.Optional;

@Repository
public interface PessoaFisicaRepository extends JpaRepository<PessoaFisicaEntity, Long> {

    Optional<PessoaFisicaEntity> findByNome(String nome);

    Optional<PessoaFisicaEntity> findByCpf(String cpf);
}
