package petter.associacao.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import petter.associacao.domain.model.EnderecoEntity;

import java.util.Optional;

@Repository
public interface EnderecoRepository extends JpaRepository<EnderecoEntity, Long> {
    @Query("select e from EnderecoEntity e inner join e.rua r where r.logradouro = :logradouro AND e.numero = :numero")
    Optional<EnderecoEntity> findByEndereco(String logradouro, Integer numero);
}
