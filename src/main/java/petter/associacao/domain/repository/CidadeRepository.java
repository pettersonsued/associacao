package petter.associacao.domain.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import petter.associacao.domain.model.CidadeEntity;
import petter.associacao.domain.model.DistritoEntity;

@Repository
public interface CidadeRepository extends JpaRepository<CidadeEntity, Long>{

	Optional<CidadeEntity> findByNome(String nome);
}
