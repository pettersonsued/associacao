package petter.associacao.domain.exception;


public class EntidadeNaoEncontradaException extends CadastroException {
	
	private static final long serialVersionUID = 1L;

	public EntidadeNaoEncontradaException(String mensagem) {
		super(mensagem);
	}

}
