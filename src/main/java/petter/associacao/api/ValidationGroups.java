package petter.associacao.api;

public interface ValidationGroups {
	
	public interface EnderecoEntityId {}
	public interface DistritoEntityId {}
	public interface CidadeEntityId {}
	public interface RuaEntityId {}
	public interface PessoaEntityId{}
	public interface MaeEntityId{}
	public interface PaiEntityId{}

	public interface AssociadoContribuinteEntityId{}

	public interface CartorioEntityId{}

	public interface EscolaEntityId{}

	public interface AlergiaEntityId{}

	public interface IrmaoEntityId{}

	public interface RegistroEntityId{}

}
