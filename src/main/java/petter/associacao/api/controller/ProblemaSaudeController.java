package petter.associacao.api.controller;


import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import petter.associacao.domain.model.AlergiaEntity;
import petter.associacao.domain.model.ProblemaSaudeEntity;
import petter.associacao.domain.service.ProblemaSaudeService;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@AllArgsConstructor
@RestController
@RequestMapping("/problemas")
public class ProblemaSaudeController {

    private ProblemaSaudeService problemaSaudeService;


    /////////////////LISTA TODAS PROBLEMAS DE SAÚDE//////////
    @GetMapping
    public List<ProblemaSaudeEntity> listar() {
        return problemaSaudeService.listar();
    }

///////////////// BUSCA PROBLEMAS DE SAÚDE POR ID///////////////////////////////////

    @GetMapping(path = {"/{id}"})
    public ResponseEntity<ProblemaSaudeEntity> buscarProblemaSaudePorId(@PathVariable("id") Long id) {
        return problemaSaudeService.findByProblemaSaudePorId(id).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    ///////////////// BUSCA PROBLEMAS DE SAÚDE POR NOME///////////////////////////////////

    @GetMapping(path = "/problema/{nome}")
    public ResponseEntity<ProblemaSaudeEntity> buscarProblemaSaudePorNome(@PathVariable("nome") String nome) {
        return problemaSaudeService.findByProblemaSaudePorNome(nome).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    ////////////////////ADICIONA UM PROBLEMAS DE SAÚDE////////////////////////

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ProblemaSaudeEntity adicionar(@Valid @RequestBody ProblemaSaudeEntity problema) {
        return problemaSaudeService.salvar(problema);
    }

    ///////////////////////////ATUALIZA PROBLEMAS DE SAÚDE/////////////////////////////

    @PutMapping(path = {"/{id}"})
    public ResponseEntity<ProblemaSaudeEntity> atualizar(@PathVariable("id") Long id, @Valid @RequestBody ProblemaSaudeEntity problema) {
        if (!problemaSaudeService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        problema.setId(id);
        problema = problemaSaudeService.atualizar(problema);
        return ResponseEntity.ok(problema);
    }

    ///////////////EXCLUI PROBLEMAS DE SAÚDE////////////////////////////////////

    @DeleteMapping(path = { "/{id}" })
    public ResponseEntity<Void> remover(@PathVariable("id") Long id) {
        if (!problemaSaudeService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        problemaSaudeService.excluir(id);
        return ResponseEntity.noContent().build();
    }
}
