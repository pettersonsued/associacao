package petter.associacao.api.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import petter.associacao.domain.model.EscolaEntity;
import petter.associacao.domain.model.MaeEntity;
import petter.associacao.domain.service.EscolaService;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@AllArgsConstructor
@RestController
@RequestMapping("/escolas")
public class EscolaControlller {

    private EscolaService escolaService;



    /////////////////LISTA TODAS AS ESCOLAS//////////
    @GetMapping
    public List<EscolaEntity> listar() {
        return escolaService.listar();
    }

///////////////// BUSCA AS ESCOLAS POR ID///////////////////////////////////

    @GetMapping(path = { "/{id}" })
    public ResponseEntity<EscolaEntity> buscarMaePorId(@PathVariable("id") Long id) {
        return escolaService.findByEscolaPorId(id).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    ///////////////// BUSCA AS ESCOLAS POR NOME///////////////////////////////////

    @GetMapping(path = "/escola/{nome}")
    public ResponseEntity<EscolaEntity> buscarMaePorNome(@PathVariable("nome") String nome) {
        return escolaService.findByEscolaPorNome(nome).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    ////////////////////ADICIONA UM AS MÃES////////////////////////

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public EscolaEntity adicionar(@Valid @RequestBody EscolaEntity escola) {
        return escolaService.salvar(escola);
    }

    ///////////////////////////ATUALIZA AS ESCOLAS/////////////////////////////

    @PutMapping(path = { "/{id}" })
    public ResponseEntity<EscolaEntity> atualizar(@PathVariable("id") Long id, @Valid @RequestBody EscolaEntity escola) {
        if (!escolaService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        escola.setId(id);
        escola = escolaService.atualizar(escola);
        return ResponseEntity.ok(escola);
    }

    ///////////////EXCLUI AS ESCOLAS////////////////////////////////////

    @DeleteMapping(path = { "/{id}" })
    public ResponseEntity<Void> remover(@PathVariable("id") Long id) {
        if (!escolaService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        escolaService.excluir(id);
        return ResponseEntity.noContent().build();
    }
}
