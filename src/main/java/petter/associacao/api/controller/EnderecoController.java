package petter.associacao.api.controller;


import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import petter.associacao.domain.model.EnderecoEntity;
import petter.associacao.domain.model.RuaEntity;
import petter.associacao.domain.service.EnderecoService;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@AllArgsConstructor
@RestController
@RequestMapping("/enderecos")
public class EnderecoController {

    private EnderecoService enderecoService;


    ///////////////////////////LISTAR EMDERECOS/////////////////////////////
    @GetMapping
    public List<EnderecoEntity> listar() {
        return enderecoService.listar();
    }


    ///////////////////////////ATUALIZA OS ENDEREÇOS/////////////////////////////
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public EnderecoEntity adicionar(@Valid @RequestBody EnderecoEntity endereco) {
        return enderecoService.salvar(endereco);
    }

    ///////////////////////////ATUALIZA OS ENDEREÇOS/////////////////////////////

    @PutMapping(path = { "/{id}" })
    public ResponseEntity<EnderecoEntity> atualizar(@PathVariable("id") Long id, @Valid @RequestBody EnderecoEntity endereco) {
        if (!enderecoService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        endereco.setId(id);
        endereco = enderecoService.atualizar(endereco);
        return ResponseEntity.ok(endereco);
    }

    ///////////////EXCLUI OS ENDEREÇOS////////////////////////////////////

    @DeleteMapping(path = { "/{id}" })
    public ResponseEntity<Void> remover(@PathVariable("id") Long id) {
        if (!enderecoService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        enderecoService.excluir(id);
        return ResponseEntity.noContent().build();
    }

}
