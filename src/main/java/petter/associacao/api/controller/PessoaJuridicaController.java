package petter.associacao.api.controller;


import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import petter.associacao.domain.model.PessoaEntity;
import petter.associacao.domain.model.PessoaJuridicaEntity;
import petter.associacao.domain.service.PessoaJuridicaService;

import javax.validation.Valid;
import java.util.List;

//@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@AllArgsConstructor
@RestController
@RequestMapping("/pessoas/juridicas")
public class PessoaJuridicaController {

    private PessoaJuridicaService pessoaJuridicaService;

//////////////////LISTA TOOS OS PESSOAS//////////

    @GetMapping
    public List<PessoaJuridicaEntity> listar() {
        return pessoaJuridicaService.listar();
    }

///////////////// BUSCA PESSOA POR ID///////////////////////////////////

    @GetMapping(path = { "/{id}" })
    public ResponseEntity<PessoaJuridicaEntity> buscarPessoaPorId(@PathVariable("id") Long id) {
        return pessoaJuridicaService.findByPessoaJuridicaPorId(id).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    ///////////////// BUSCA CIDADE POR NOME///////////////////////////////////

    @GetMapping(path = "/juridica/{nome}")
    public ResponseEntity<PessoaJuridicaEntity> buscarPessoaPorNome(@PathVariable("nome") String nome) {
        return pessoaJuridicaService.findByPessoaJuridicaPorNome(nome).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

////////////////////ADICIONA UM CIDADE////////////////////////

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PessoaJuridicaEntity adicionar(@Valid @RequestBody PessoaJuridicaEntity pessoa) {
        return pessoaJuridicaService.salvar(pessoa);
    }

    ///////////////////////////ATUALIZA OS CIDADES/////////////////////////////

    @PutMapping(path = { "/{id}" })
    public ResponseEntity<PessoaJuridicaEntity> atualizar(@PathVariable("id") Long id, @Valid @RequestBody PessoaJuridicaEntity pessoaJuridica) {
        if (!pessoaJuridicaService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        pessoaJuridica.setId(id);
        pessoaJuridica = pessoaJuridicaService.atualizar(pessoaJuridica);
        return ResponseEntity.ok(pessoaJuridica);
    }

///////////////EXCLUI OS CIDADES////////////////////////////////////

    @DeleteMapping(path = { "/{id}" })
    public ResponseEntity<Void> remover(@PathVariable("id") Long id) {
        if (!pessoaJuridicaService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        pessoaJuridicaService.excluir(id);
        return ResponseEntity.noContent().build();
    }
}
