package petter.associacao.api.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import petter.associacao.domain.model.AlunoEntity;
import petter.associacao.domain.model.PaiEntity;
import petter.associacao.domain.service.AlunoService;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@AllArgsConstructor
@RestController
@RequestMapping("/alunos")
public class AlunoController {

    private AlunoService alunoService;



    /////////////////LISTA TODAS OS ALUNOS//////////
    @GetMapping
    public List<AlunoEntity> listar() {
        return alunoService.listar();
    }

///////////////// BUSCA OS ALUNOS POR ID///////////////////////////////////

    @GetMapping(path = { "/{id}" })
    public ResponseEntity<AlunoEntity> buscarPaiPorId(@PathVariable("id") Long id) {
        return alunoService.findByAlunoPorId(id).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    ///////////////// BUSCA OS ALUNOS POR NOME///////////////////////////////////

    @GetMapping(path = "/aluno/{nome}")
    public ResponseEntity<AlunoEntity> buscarAlunoPorNome(@PathVariable("nome") String nome) {
        return alunoService.findByAlunoPorNome(nome).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    ////////////////////ADICIONA UM OS ALUNOS////////////////////////

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AlunoEntity adicionar(@Valid @RequestBody AlunoEntity aluno) {
        return alunoService.salvar(aluno);
    }

    ///////////////////////////ATUALIZA OS ALUNOS/////////////////////////////

    @PutMapping(path = { "/{id}" })
    public ResponseEntity<AlunoEntity> atualizar(@PathVariable("id") Long id, @Valid @RequestBody AlunoEntity aluno) {
        if (!alunoService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        aluno.setId(id);
        aluno = alunoService.atualizar(aluno);
        return ResponseEntity.ok(aluno);
    }

    ///////////////EXCLUI OS ALUNOS////////////////////////////////////

    @DeleteMapping(path = { "/{id}" })
    public ResponseEntity<Void> remover(@PathVariable("id") Long id) {
        if (!alunoService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        alunoService.excluir(id);
        return ResponseEntity.noContent().build();
    }
}
