package petter.associacao.api.controller;


import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import petter.associacao.domain.model.MaeEntity;
import petter.associacao.domain.model.PaiEntity;
import petter.associacao.domain.service.PaiService;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@AllArgsConstructor
@RestController
@RequestMapping("/pais")
public class PaiController {

    private PaiService paiService;




    /////////////////LISTA TODAS OS PAIS//////////
    @GetMapping
    public List<PaiEntity> listar() {
        return paiService.listar();
    }

///////////////// BUSCA OS PAIS POR ID///////////////////////////////////

    @GetMapping(path = { "/{id}" })
    public ResponseEntity<PaiEntity> buscarPaiPorId(@PathVariable("id") Long id) {
        return paiService.findByPaiPorId(id).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    ///////////////// BUSCA OS PAIS POR NOME///////////////////////////////////

    @GetMapping(path = "/pai/{nome}")
    public ResponseEntity<PaiEntity> buscarPaiPorNome(@PathVariable("nome") String nome) {
        return paiService.findByPaiPorNome(nome).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    ////////////////////ADICIONA UM OS PAISS////////////////////////

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PaiEntity adicionar(@Valid @RequestBody PaiEntity pai) {
        return paiService.salvar(pai);
    }

    ///////////////////////////ATUALIZA OS PAIS/////////////////////////////

    @PutMapping(path = { "/{id}" })
    public ResponseEntity<PaiEntity> atualizar(@PathVariable("id") Long id, @Valid @RequestBody PaiEntity pai) {
        if (!paiService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        pai.setId(id);
        pai = paiService.atualizar(pai);
        return ResponseEntity.ok(pai);
    }

    ///////////////EXCLUI OS PAIS////////////////////////////////////

    @DeleteMapping(path = { "/{id}" })
    public ResponseEntity<Void> remover(@PathVariable("id") Long id) {
        if (!paiService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        paiService.excluir(id);
        return ResponseEntity.noContent().build();
    }
}
