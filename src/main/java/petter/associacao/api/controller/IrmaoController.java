package petter.associacao.api.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import petter.associacao.domain.model.IrmaoEntity;
import petter.associacao.domain.model.MaeEntity;
import petter.associacao.domain.service.IrmaoService;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@AllArgsConstructor
@RestController
@RequestMapping("/irmaos")
public class IrmaoController {

    private IrmaoService irmaoService;




    /////////////////LISTA TODAS IRMÃO//////////
    @GetMapping
    public List<IrmaoEntity> listar() {
        return irmaoService.listar();
    }

///////////////// BUSCA IRMÃO POR ID///////////////////////////////////

    @GetMapping(path = { "/{id}" })
    public ResponseEntity<IrmaoEntity> buscarIrmaoPorId(@PathVariable("id") Long id) {
        return irmaoService.findByIrmaoPorId(id).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    ///////////////// BUSCA IRMÃO POR NOME///////////////////////////////////

    @GetMapping(path = "/irmao/{nome}")
    public ResponseEntity<IrmaoEntity> buscarIrmaoPorNome(@PathVariable("nome") String nome) {
        return irmaoService.findByIrmaoPorNome(nome).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    ////////////////////ADICIONA UM IRMÃO////////////////////////

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public IrmaoEntity adicionar(@Valid @RequestBody IrmaoEntity irmao) {
        return irmaoService.salvar(irmao);
    }

    ///////////////////////////ATUALIZA IRMÃO/////////////////////////////

    @PutMapping(path = { "/{id}" })
    public ResponseEntity<IrmaoEntity> atualizar(@PathVariable("id") Long id, @Valid @RequestBody IrmaoEntity irmao) {
        if (!irmaoService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        irmao.setId(id);
        irmao = irmaoService.atualizar(irmao);
        return ResponseEntity.ok(irmao);
    }

    ///////////////EXCLUI IRMÃO////////////////////////////////////

    @DeleteMapping(path = { "/{id}" })
    public ResponseEntity<Void> remover(@PathVariable("id") Long id) {
        if (!irmaoService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        irmaoService.excluir(id);
        return ResponseEntity.noContent().build();
    }
}
