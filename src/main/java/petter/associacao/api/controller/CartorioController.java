package petter.associacao.api.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import petter.associacao.domain.model.CartorioEntity;
import petter.associacao.domain.model.MaeEntity;
import petter.associacao.domain.service.CartorioService;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@AllArgsConstructor
@RestController
@RequestMapping("/cartorios")
public class CartorioController {

    private CartorioService cartorioService;



    /////////////////LISTA TODAS OS CARTORIOS//////////
    @GetMapping
    public List<CartorioEntity> listar() {
        return cartorioService.listar();
    }

///////////////// BUSCA OS CARTORIOS POR ID///////////////////////////////////

    @GetMapping(path = { "/{id}" })
    public ResponseEntity<CartorioEntity> buscarCartorioPorId(@PathVariable("id") Long id) {
        return cartorioService.findByCartorioPorId(id).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    ///////////////// BUSCA OS CARTORIOS POR NOME///////////////////////////////////

    @GetMapping(path = "/cartorio/{nome}")
    public ResponseEntity<CartorioEntity> buscarCartorioPorNome(@PathVariable("nome") String nome) {
        return cartorioService.findByCartorioPorNome(nome).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    ////////////////////ADICIONA UM OS CARTORIOS////////////////////////

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CartorioEntity adicionar(@Valid @RequestBody CartorioEntity cartorio) {
        return cartorioService.salvar(cartorio);
    }

    ///////////////////////////ATUALIZA OS CARTORIOS/////////////////////////////

    @PutMapping(path = { "/{id}" })
    public ResponseEntity<CartorioEntity> atualizar(@PathVariable("id") Long id, @Valid @RequestBody CartorioEntity cartorio) {
        if (!cartorioService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        cartorio.setId(id);
        cartorio = cartorioService.atualizar(cartorio);
        return ResponseEntity.ok(cartorio);
    }

    ///////////////EXCLUI AS MÃES////////////////////////////////////

    @DeleteMapping(path = { "/{id}" })
    public ResponseEntity<Void> remover(@PathVariable("id") Long id) {
        if (!cartorioService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        cartorioService.excluir(id);
        return ResponseEntity.noContent().build();
    }
}
