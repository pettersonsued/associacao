package petter.associacao.api.controller;


import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import petter.associacao.domain.model.PessoaEntity;
import petter.associacao.domain.model.PessoaFisicaEntity;
import petter.associacao.domain.model.PessoaJuridicaEntity;
import petter.associacao.domain.service.PessoaFisicaService;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@AllArgsConstructor
@RestController
@RequestMapping("/pessoas/fisicas")
public class PessoaFisicaController {

    private PessoaFisicaService pessoaFisicaService;



    /////////////////LISTA TOOS OS PESSOAS FISICAS//////////

    @GetMapping
    public List<PessoaFisicaEntity> listar() {
        return pessoaFisicaService.listar();
    }

///////////////// BUSCA PESSOAS FISICAS POR ID///////////////////////////////////

    @GetMapping(path = { "/{id}" })
    public ResponseEntity<PessoaFisicaEntity> buscarPessoaPorId(@PathVariable("id") Long id) {
        return pessoaFisicaService.findByPessoaFisicaPorId(id).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    ///////////////// BUSCA PESSOAS FISICAS POR NOME///////////////////////////////////

    @GetMapping(path = "/fisica/{nome}")
    public ResponseEntity<PessoaFisicaEntity> buscarPessoaPorNome(@PathVariable("nome") String nome) {
        return pessoaFisicaService.findByPessoaFisicaPorNome(nome).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    ////////////////////ADICIONA UM PESSOAS FISICAS////////////////////////

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PessoaFisicaEntity adicionar(@Valid @RequestBody PessoaFisicaEntity pessoa) {
        return pessoaFisicaService.salvar(pessoa);
    }

    ///////////////////////////ATUALIZA AS PESSOAS FISICAS/////////////////////////////

    @PutMapping(path = { "/{id}" })
    public ResponseEntity<PessoaFisicaEntity> atualizar(@PathVariable("id") Long id, @Valid @RequestBody PessoaFisicaEntity pessoaFisica) {
        if (!pessoaFisicaService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        pessoaFisica.setId(id);
        pessoaFisica = pessoaFisicaService.atualizar(pessoaFisica);
        return ResponseEntity.ok(pessoaFisica);
    }

    ///////////////EXCLUI AS PESSOAS FISICAS////////////////////////////////////

    @DeleteMapping(path = { "/{id}" })
    public ResponseEntity<Void> remover(@PathVariable("id") Long id) {
        if (!pessoaFisicaService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        pessoaFisicaService.excluir(id);
        return ResponseEntity.noContent().build();
    }
}

