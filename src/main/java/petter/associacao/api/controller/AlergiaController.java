package petter.associacao.api.controller;


import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import petter.associacao.domain.model.AlergiaEntity;
import petter.associacao.domain.model.MaeEntity;
import petter.associacao.domain.service.AlergiaService;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@AllArgsConstructor
@RestController
@RequestMapping("/alergias")
public class AlergiaController {

    private AlergiaService alergiaService;




    /////////////////LISTA TODAS AS ALERGIA//////////
    @GetMapping
    public List<AlergiaEntity> listar() {
        return alergiaService.listar();
    }

///////////////// BUSCA AS ALERGIA POR ID///////////////////////////////////

    @GetMapping(path = { "/{id}" })
    public ResponseEntity<AlergiaEntity> buscarAlergiaPorId(@PathVariable("id") Long id) {
        return alergiaService.findByAlergiaPorId(id).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    ///////////////// BUSCA AS ALERGIA POR NOME///////////////////////////////////

    @GetMapping(path = "/alergia/{nome}")
    public ResponseEntity<AlergiaEntity> buscarAlergiaPorNome(@PathVariable("nome") String nome) {
        return alergiaService.findByAlergiaPorNome(nome).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    ////////////////////ADICIONA UM AS ALERGIA////////////////////////

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AlergiaEntity adicionar(@Valid @RequestBody AlergiaEntity alergia) {
        return alergiaService.salvar(alergia);
    }

    ///////////////////////////ATUALIZA AS ALERGIA/////////////////////////////

    @PutMapping(path = { "/{id}" })
    public ResponseEntity<AlergiaEntity> atualizar(@PathVariable("id") Long id, @Valid @RequestBody AlergiaEntity alergia) {
        if (!alergiaService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        alergia.setId(id);
        alergia = alergiaService.atualizar(alergia);
        return ResponseEntity.ok(alergia);
    }

    ///////////////EXCLUI AS ALERGIA////////////////////////////////////

    @DeleteMapping(path = { "/{id}" })
    public ResponseEntity<Void> remover(@PathVariable("id") Long id) {
        if (!alergiaService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        alergiaService.excluir(id);
        return ResponseEntity.noContent().build();
    }
}
