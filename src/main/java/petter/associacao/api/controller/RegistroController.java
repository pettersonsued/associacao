package petter.associacao.api.controller;


import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import petter.associacao.domain.model.PaiEntity;
import petter.associacao.domain.model.RegistroEntity;
import petter.associacao.domain.service.RegistroService;

import javax.validation.Valid;
import java.util.List;

//@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@AllArgsConstructor
@RestController
@RequestMapping("/registros")
public class RegistroController {

    private RegistroService registroService;


    /////////////////LISTA TODAS OS REGISTROS//////////
    @GetMapping
    public List<RegistroEntity> listar() {
        return registroService.listar();
    }

///////////////// BUSCA OS REGISTROS POR ID///////////////////////////////////

    @GetMapping(path = { "/{id}" })
    public ResponseEntity<RegistroEntity> buscarPaiPorId(@PathVariable("id") Long id) {
        return registroService.findByRegistroPorId(id).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    ///////////////// BUSCA OS REGISTROS POR NOME///////////////////////////////////

   /* @GetMapping(path = "/pai/{nome}")
    public ResponseEntity<RegistroEntity> buscarPaiPorNome(@PathVariable("nome") String nome) {
        return registroService.findByPaiPorNome(nome).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }*/

    ////////////////////ADICIONA UM OS REGISTROS////////////////////////

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RegistroEntity adicionar(@Valid @RequestBody RegistroEntity registro) {
        return registroService.salvar(registro);
    }

    ///////////////////////////ATUALIZA OS REGISTROS/////////////////////////////

    /*@PutMapping(path = { "/{id}" })
    public ResponseEntity<RegistroEntity> atualizar(@PathVariable("id") Long id, @Valid @RequestBody RegistroEntity registro) {
        if (!registroService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        registro.setId(id);
        registro = registroService.atualizar(registro);
        return ResponseEntity.ok(registro);
    }*/

    ///////////////EXCLUI OS REGISTROS////////////////////////////////////

    @DeleteMapping(path = { "/{id}" })
    public ResponseEntity<Void> remover(@PathVariable("id") Long id) {
        if (!registroService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        registroService.excluir(id);
        return ResponseEntity.noContent().build();
    }
}
