package petter.associacao.api.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import petter.associacao.domain.model.MaeEntity;
import petter.associacao.domain.model.PessoaFisicaEntity;
import petter.associacao.domain.service.MaeService;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@AllArgsConstructor
@RestController
@RequestMapping("/maes")
public class MaeController {

    private MaeService maeService;



    /////////////////LISTA TODAS AS MÃES//////////
    @GetMapping
    public List<MaeEntity> listar() {
        return maeService.listar();
    }

///////////////// BUSCA AS MÃES POR ID///////////////////////////////////

    @GetMapping(path = { "/{id}" })
    public ResponseEntity<MaeEntity> buscarMaePorId(@PathVariable("id") Long id) {
        return maeService.findByMaePorId(id).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    ///////////////// BUSCA AS MÃES POR NOME///////////////////////////////////

    @GetMapping(path = "/mae/{nome}")
    public ResponseEntity<MaeEntity> buscarMaePorNome(@PathVariable("nome") String nome) {
        return maeService.findByMaePorNome(nome).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    ////////////////////ADICIONA UM AS MÃES////////////////////////

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public MaeEntity adicionar(@Valid @RequestBody MaeEntity mae) {
        return maeService.salvar(mae);
    }

    ///////////////////////////ATUALIZA AS MÃES/////////////////////////////

    @PutMapping(path = { "/{id}" })
    public ResponseEntity<MaeEntity> atualizar(@PathVariable("id") Long id, @Valid @RequestBody MaeEntity mae) {
        if (!maeService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        mae.setId(id);
        mae = maeService.atualizar(mae);
        return ResponseEntity.ok(mae);
    }

    ///////////////EXCLUI AS MÃES////////////////////////////////////

    @DeleteMapping(path = { "/{id}" })
    public ResponseEntity<Void> remover(@PathVariable("id") Long id) {
        if (!maeService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        maeService.excluir(id);
        return ResponseEntity.noContent().build();
    }
}
