package petter.associacao.api.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import lombok.AllArgsConstructor;
import petter.associacao.domain.model.CidadeEntity;
import petter.associacao.domain.model.RuaEntity;
import petter.associacao.domain.service.RuaService;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@AllArgsConstructor
@RestController
@RequestMapping("/ruas")
public class RuaController {
	
	private RuaService ruaService;
	
	@GetMapping
	public List<RuaEntity> listar() {
		return ruaService.listar();
	}

	///////////////// BUSCA RUA POR ID///////////////////////////////////

	@GetMapping(path = { "/{id}" })
	public ResponseEntity<RuaEntity> buscarRuaPorId(@PathVariable("id") Long id) {
		return ruaService.findByRuaPorId(id).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
	}

	///////////////// BUSCA RUA POR NOME///////////////////////////////////

	@GetMapping(path = { "/rua/{name}" })
	public ResponseEntity<RuaEntity> buscarRuaPorNome(@PathVariable("name") String name) {
		return ruaService.findByRuaPorNome(name).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public RuaEntity adicionar(@Valid @RequestBody RuaEntity rua) {
		return ruaService.salvar(rua);
	}

	///////////////////////////ATUALIZA OS CIDADES/////////////////////////////

	@PutMapping(path = { "/{id}" })
	public ResponseEntity<RuaEntity> atualizar(@PathVariable("id") Long id, @Valid @RequestBody RuaEntity rua) {
		if (!ruaService.existsById(id)) {
			return ResponseEntity.notFound().build();
		}
		rua.setId(id);
		rua = ruaService.atualizar(rua);
		return ResponseEntity.ok(rua);
	}

	///////////////EXCLUI AS RUAS////////////////////////////////////

	@DeleteMapping(path = { "/{id}" })
	public ResponseEntity<Void> remover(@PathVariable("id") Long id) {
		if (!ruaService.existsById(id)) {
			return ResponseEntity.notFound().build();
		}
		ruaService.excluir(id);
		return ResponseEntity.noContent().build();
	}
}
