package petter.associacao.api.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import lombok.AllArgsConstructor;
import petter.associacao.domain.model.CidadeEntity;
import petter.associacao.domain.model.DistritoEntity;
import petter.associacao.domain.service.CidadeService;
import petter.associacao.domain.service.DistritoService;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@AllArgsConstructor
@RestController
@RequestMapping("/distritos")
public class DistritoController {

	private DistritoService distritoService;
	
	@GetMapping
	public List<DistritoEntity> listar() {
		return distritoService.listar();
	}
	
	@GetMapping(path = { "/{id}" })
	public ResponseEntity<DistritoEntity> buscarDistritoPorId(@PathVariable("id") Long id) {
		return distritoService.findByDistritoPorId(id).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
	}

	///////////////// BUSCA DISTRITO POR NOME///////////////////////////////////

	@GetMapping(path = { "/distrito/{name}" })
	public ResponseEntity<DistritoEntity> buscarDistritoPorNome(@PathVariable("name") String name) {
		return distritoService.findByDistritoPorNome(name).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
	}
	
	
	////////////////////ADICIONAR DISTRITO/////////////////////////////////
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public DistritoEntity adicionar(@Valid @RequestBody DistritoEntity distrito) {
		return distritoService.salvar(distrito);
	}
	

	///////////////////////////ATUALIZA OS DISTRITOS/////////////////////////////

	@PutMapping(path = { "/{id}" })
	public ResponseEntity<DistritoEntity> atualizar(@PathVariable("id") Long id, @Valid @RequestBody DistritoEntity distrito) {
		if (!distritoService.existsById(id)) {
			return ResponseEntity.notFound().build();
		}
		distrito.setId(id);
		distrito = distritoService.atualizar(distrito);
		return ResponseEntity.ok(distrito);
	}

	///////////////EXCLUI OS DISTRITOS////////////////////////////////////

	@DeleteMapping(path = { "/{id}" })
	public ResponseEntity<Void> remover(@PathVariable("id") Long id) {
		if (!distritoService.existsById(id)) {
			return ResponseEntity.notFound().build();
		}
		distritoService.excluir(id);
		return ResponseEntity.noContent().build();
	}
}
