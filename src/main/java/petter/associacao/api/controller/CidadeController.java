package petter.associacao.api.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import lombok.AllArgsConstructor;
import petter.associacao.domain.model.CidadeEntity;
import petter.associacao.domain.service.CidadeService;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@AllArgsConstructor
@RestController
@RequestMapping("/cidades")
public class CidadeController {

	private CidadeService cidadeService;

//////////////////LISTA TOOS OS CIDADES//////////

	@GetMapping
	public List<CidadeEntity> listar() {
		return cidadeService.listar();
	}

///////////////// BUSCA CIDADE POR ID///////////////////////////////////

	@GetMapping(path = { "/{id}" })
	public ResponseEntity<CidadeEntity> buscarCidadePorId(@PathVariable("id") Long id) {
		return cidadeService.findByCidadePorId(id).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
	}
	

	///////////////// BUSCA CIDADE POR NOME///////////////////////////////////
	
	@GetMapping(path = "/cidade/{nome}")
	public ResponseEntity<CidadeEntity> buscarCidadePorNome(@PathVariable("nome") String nome) {
		return cidadeService.findByCidadePorNome(nome).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
	}

////////////////////ADICIONA UM CIDADE////////////////////////

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public CidadeEntity adicionar(@Valid @RequestBody CidadeEntity cidade) {
		return cidadeService.salvar(cidade);
	}

///////////////////////////ATUALIZA OS CIDADES/////////////////////////////

	@PutMapping(path = { "/{id}" })
	public ResponseEntity<CidadeEntity> atualizar(@PathVariable("id") Long id, @Valid @RequestBody CidadeEntity cidade) {
		if (!cidadeService.existsById(id)) {
			return ResponseEntity.notFound().build();
		}
		cidade.setId(id);
		cidade = cidadeService.atualizar(cidade);
		return ResponseEntity.ok(cidade);
	}

///////////////EXCLUI OS CIDADES////////////////////////////////////

	@DeleteMapping(path = { "/{id}" })
	public ResponseEntity<Void> remover(@PathVariable("id") Long id) {
		if (!cidadeService.existsById(id)) {
			return ResponseEntity.notFound().build();
		}
		cidadeService.excluir(id);
		return ResponseEntity.noContent().build();
	}

}
