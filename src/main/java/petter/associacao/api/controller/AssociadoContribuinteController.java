package petter.associacao.api.controller;


import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import petter.associacao.domain.model.AssociadoContribuinteEntity;
import petter.associacao.domain.model.MaeEntity;
import petter.associacao.domain.service.AssociadoContribuinteService;

import javax.validation.Valid;
import java.util.List;

//@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@AllArgsConstructor
@RestController
@RequestMapping("contribuintes")
public class AssociadoContribuinteController {

    private AssociadoContribuinteService associadoContribuinteService;


    /////////////////LISTA TODOS OS ASSOCIADOS CONTRIBUINTE//////////
    @GetMapping
    public List<AssociadoContribuinteEntity> listar() {
        return associadoContribuinteService.listar();
    }

///////////////// BUSCA OS ASSOCIADOS CONTRIBUINTE POR ID///////////////////////////////////

    @GetMapping(path = { "/{id}" })
    public ResponseEntity<AssociadoContribuinteEntity> buscarContribuintePorId(@PathVariable("id") Long id) {
        return associadoContribuinteService.findByContribuintePorId(id).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    ///////////////// BUSCA OS ASSOCIADOS CONTRIBUINTE POR NOME///////////////////////////////////

   @GetMapping(path = "/contribuinte/{nome}")
    public ResponseEntity<AssociadoContribuinteEntity> buscarContribuintePorNome(@PathVariable("nome") String nome) {
        return associadoContribuinteService.findByContribuintePorNome(nome).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    ////////////////////ADICIONA UM OS ASSOCIADOS CONTRIBUINTE////////////////////////

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AssociadoContribuinteEntity adicionar(@Valid @RequestBody AssociadoContribuinteEntity contribuinte) {
        return associadoContribuinteService.salvar(contribuinte);
    }

    ///////////////////////////ATUALIZA OS ASSOCIADOS CONTRIBUINTE/////////////////////////////

    @PutMapping(path = { "/{id}" })
    public ResponseEntity<AssociadoContribuinteEntity> atualizar(@PathVariable("id") Long id, @Valid @RequestBody AssociadoContribuinteEntity contribuinte) {
        if (!associadoContribuinteService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        contribuinte.setId(id);
        contribuinte = associadoContribuinteService.atualizar(contribuinte);
        return ResponseEntity.ok(contribuinte);
    }

    ///////////////EXCLUI OS ASSOCIADOS CONTRIBUINTE////////////////////////////////////

    @DeleteMapping(path = { "/{id}" })
    public ResponseEntity<Void> remover(@PathVariable("id") Long id) {
        if (!associadoContribuinteService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        associadoContribuinteService.excluir(id);
        return ResponseEntity.noContent().build();
    }
}
